<?php

namespace App\Http\Controllers;

use DB;
// use App\Http\Requests;
use Illuminate\Http\Request;
// use App\Services\Test\Test;
use App\Services\Users\User;
use App\Services\Users\UserObject;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\Reserve\Reserve;
use App\Services\Rest\Rest;
use App\Services\Stat\Stat;

class AdminController extends Controller
{
    public function getDashboard()
    {
    	$userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

    	$getpets = Pet::where('user_id',$userObject->id)->get(); 

    	$getstatDay = Stat::where('day', date("d"));
		$getstatDay = $getstatDay->groupBy('day');
    	$getstatDay = $getstatDay->select('day', DB::raw('sum(count) as day_count'));
		$getstatDay = $getstatDay->first();

		$getstatMonth = Stat::where('month', date("F"));
		$getstatMonth = $getstatMonth->groupBy('month');
    	$getstatMonth = $getstatMonth->select('month', DB::raw('sum(count) as month_count'));
		$getstatMonth = $getstatMonth->first();

		$getstatYear = Stat::where('year', date("Y"));
		$getstatYear = $getstatYear->groupBy('year');
    	$getstatYear = $getstatYear->select('year', DB::raw('sum(count) as year_count'));
		$getstatYear = $getstatYear->first();

		// d($getstatDay->toArray());
		// d($getstatMonth->toArray());
		// sd($getstatYear->toArray());
        // $reserve_1 = Reserve::join('tbl_payment', function ($join) {
        //     $join->on('tbl_payment.pay_id', '=', 'tbl_reserve.pay_id')
        //          ->where('tbl_reserve.rest_id', '=',  1)
        //          ->where('tbl_payment.pay_status', '=', 'completed');
        // })->get();

        // $reserve_2 = Reserve::join('tbl_payment', function ($join) {
        //     $join->on('tbl_payment.pay_id', '=', 'tbl_reserve.pay_id')
        //          ->where('tbl_reserve.rest_id', '=',  2)
        //          ->where('tbl_payment.pay_status', '=', 'completed');
        // })->get();

        // $reserve_3 = Reserve::join('tbl_payment', function ($join) {
        //     $join->on('tbl_payment.pay_id', '=', 'tbl_reserve.pay_id')
        //          ->where('tbl_reserve.rest_id', '=',  3)
        //          ->where('tbl_payment.pay_status', '=', 'completed');
        // })->get();
        
		$reserve_1 = Reserve::where('rest_id', 1)->get();
        $reserve_2 = Reserve::where('rest_id', 2)->get();
        $reserve_3 = Reserve::where('rest_id', 3)->get();

        $count_1 = $reserve_1->count();
        $count_2 = $reserve_2->count();
        $count_3 = $reserve_3->count();

        $total_1 = 30 - $count_1;
        $total_2 = 5 - $count_2;
        $total_3 = 6 - $count_3;

		$getlastbooking = Reserve::with(['payment','user','rest'])->orderBy('reserve_id', 'desc')->take(5)->get();
    	
        return $this->view('admin.dashboard',compact('userObject','getpets', 'getstatDay', 'getstatMonth', 'getstatYear', 'getlastbooking', 'total_1', 'total_2', 'total_3'));
    }
    


    public function getDetail()
    {
        $payments = Reserve::with(['payment','user','rest'])->get();

    	// sd($payments->toArray());

        return $this->view('admin.detail', compact('payments'));
    }


    public function getPaymentDetail(Request $request, $id)
    {
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';
        // sd($userObject);
        if(!empty($userObject)){
            $reserve = Reserve::with(['rest','user'])->where('reserve_id', $id)->first();

            $pets = Pet::get();

            $reserve_1 = Reserve::where('rest_id', $reserve->rest_id)->get();

            $count_1 = $reserve_1->count();

            if($reserve->rest_id == 1){
                $name_no = "CAGE_00".$count_1;
            }elseif ($reserve->rest_id == 2){
                $name_no = "STALL_00".$count_1;
            }elseif ($reserve->rest_id == 3) {
                $name_no = "FS_00".$count_1;
            }
        }

        return $this->view('admin.payment_detail',compact('userObject','reserve','pets','name_no'));
    }


    public function postRemoveReserve(){

		$delete_id 	 = \Input::has('remove_id') ? \Input::get('remove_id') : '';

		$reserve = Reserve::where('reserve_id',$delete_id)->first();

		if(empty($reserve)) return helperReturnErrorFormRequest('Not found Data.');
        
		$reserve->delete();
	
		
        return ['status' => 'success'];
    }


    public function ajaxCenter(){
    	$method   = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {
            case 'pendingOrCompletePetCome':
                
                if (!\Input::has('pet_come_id')) return ['status' => 'error', 'msg' => 'Not found this reserve!!'];

				$pet_come_id = \Input::get('pet_come_id');
				$new_staus 	 = \Input::get('status_change');
				
				$reserve = Reserve::where('reserve_id',$pet_come_id)->first();

				if (!empty($reserve)){
					$reserve->pet_status_update = ($new_staus != 'false') ? "completed" : "pending";
					$reserve->save();

					return ['status' => 'success'];
				}
					return ['status' => 'error', 'msg' => 'Not found this reserve!!'];
				break;

			case 'pendingOrCompletePetReturn':
                
                if (!\Input::has('pet_return_id')) return ['status' => 'error', 'msg' => 'Not found this reserve!!'];

				$pet_return_id = \Input::get('pet_return_id');
				$new_staus 	 = \Input::get('status_change');
				
				$reserve = Reserve::where('reserve_id',$pet_return_id)->first();

				if (!empty($reserve)){
					$reserve->pet_return_status = ($new_staus != 'false') ? "completed" : "pending";
					$reserve->save();

					return ['status' => 'success'];
				}
					return ['status' => 'error', 'msg' => 'Not found this reserve!!'];
				break;

			case 'postApporvePayment':
                
                if (!\Input::has('pay_id')) return ['status' => 'error', 'msg' => 'Not found this payment!!'];

				$pay_id = \Input::get('pay_id');
				
				$payment = Payment::where('pay_id',$pay_id)->first();

				if (!empty($payment)){
					$payment->pay_status = "completed";
					$payment->save();

					return ['status' => 'success'];
				}
					return ['status' => 'error', 'msg' => 'Not found this payment!!'];
				break;


            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
        
    }
        
   

}