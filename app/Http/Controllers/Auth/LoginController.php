<?php

namespace App\Http\Controllers\Auth;

use App;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        if (UserAuth::check()) {
            if(UserAuth::check_user_type()){

                // sd(UserAuth::check_user_type());

                return \Redirect::route('main.index.get');
            }else{
                return \Redirect::route('main.index.get');

            }
        }

        // sd(UserAuth::check());

        return $this->view('auth.login');
    }

     public function postLogin()
    {

        $username = \Input::has('username') ? \Input::get('username') : '';
        $password = \Input::has('password') ? \Input::get('password') : '';

        $password =  md5($password);

        if ($user = UserAuth::attempt($username, $password)) {

        //     if($user->user_type == 'admin'){
        //         return \Redirect::route('main.index.get');
        //     }else{
        //         return \Redirect::route('main.index.get');
        //     }
            return ['status' => 'success', 'user_type' => $user->user_type];

        }else{
            return ['status' => 'error', 'msg' => 'Invalid Username or Password.'];
        }
        // sd($user);

        // if(empty($user)) 

        

        // Redirect to Login Page.
        // return redirect("/auth/login")->withInput(\Input::except('password'))
        //         ->withErrors(['username' => 'Invalid Username or Password']);
        // $user   = User::where('username', $username)->where('password', $password)->first();

        // $login_page = 'auth.login';
        // return view($login_page);

    }

        public function logout()
    {
        UserAuth::clear();

        return \Redirect::route('auth.login.get');
    }
}
