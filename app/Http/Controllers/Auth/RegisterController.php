<?php

namespace App\Http\Controllers\Auth;

// use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;


class RegisterController extends Controller
{
    public function getRegister(){

        // เรียกหน้า view register
        return $this->view('auth.register');
    }


    // function สำหรับ save ข้อมูลลง database 
    public function postRegister(){
        $username   = \Input::has('username') ? \Input::get('username') : '';
        $password   = \Input::has('password') ? \Input::get('password') : '';
        $email      = \Input::has('email') ? \Input::get('email') : '';
        $firstname  = \Input::has('firstname') ? \Input::get('firstname') : '';
        $lastname   = \Input::has('lastname') ? \Input::get('lastname') : '';
        $tel        = \Input::has('tel') ? \Input::get('tel') : '';
        $line_id    = \Input::has('line_id') ? \Input::get('line_id') : '';

        // sd($email);

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        // $chk_username   = User::where('username', $username)->count();
        // ถ้ามีก็ return error กลับไป
        // if($chk_username >= 1) return ['status' => 'error', 'msg' => 'Username is ready exits.'];

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_email  = User::where('email', $email)->count();
        // ถ้ามีก็ return error กลับไป
        if($chk_email >= 1) return ['status' => 'error', 'msg' => 'Email is ready exits.'];

       //  d($username);
       //  d($password);
       //  d($email);
       //  d($firstname);
       //  d($lastname);
       //  d($sex);
       //  d($birthday);
       //  d($address);
       // sd($tel);
        
        // save ข้อมูลลง database
        $user               = new User;
        $user->firstname    = $firstname;
        $user->lastname     = $lastname;
        $user->username     = $username;
        $user->password     = md5($password);
        $user->user_type    = 'user';
        $user->email        = $email;
        $user->tel          = $tel;
        $user->line_id      = $line_id;
        $user->save();

        return ['status' => 'success'];
    }
}
