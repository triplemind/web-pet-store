<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Services\Users\User;
use App\Services\Users\UserObject;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\Reserve\Reserve;
use App\Services\Rest\Rest;
use App\Services\Stat\Stat;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function view($page, $data = [])
    {
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        // sd($userObject);

        // $getdates = Reserve::get();

        $getdates = Reserve::join('tbl_payment', function ($join) {
            $join->on('tbl_payment.pay_id', '=', 'tbl_reserve.pay_id')
                 ->where('tbl_payment.pay_status', '=', 'pending');
        })->get();

        // d($getdates->toArray());

        foreach ($getdates as $key => $getdate) {

	        $startdate = date(strtotime($getdate->created_at));
			$expire = strtotime('+1 days', $startdate);
			$today = strtotime("today midnight");

			// d($getdate->created_at);

			// d($startdate);
			// d($expire);
			// sd($today);

			if($today >= $expire){
			    // sd("expired") ;
			    $getdate->delete();
			} 

        }


        $layout     = 'layouts/template';

        return view($layout, compact('page', 'data', 'userObject'));
    }

}
