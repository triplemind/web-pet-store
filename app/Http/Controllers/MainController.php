<?php

namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;
// use App\Services\Test\Test;
use App\Services\Users\User;
use App\Services\Users\UserObject;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\Reserve\Reserve;
use App\Services\Rest\Rest;
use App\Services\Stat\Stat;

class MainController extends Controller
{
	public function Index()
	{
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        $getpets = "";

        if(!empty($userObject)){
            $getpets = Pet::where('user_id',$userObject->id)->get(); 
        }

		return $this->view('main.index',compact('userObject','getpets'));
	}


    public function getService()
    {

        return $this->view('service.index');
    }
    

    public function getProfile()
    {

        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        // get data user เพื่อส่งไปแสดงหน้า member
        $member = User::where('id',$userObject->id)->first();

        return $this->view('main.member',compact('member'));
    }

    public function postEditProfile()
    {
        $id         = \Input::has('id') ? \Input::get('id') : '';
        $username   = \Input::has('username') ? \Input::get('username') : '';
        $password   = \Input::has('password') ? \Input::get('password') : '';
        $firstname  = \Input::has('firstname') ? \Input::get('firstname') : '';
        $lastname   = \Input::has('lastname') ? \Input::get('lastname') : '';
        $email      = \Input::has('email') ? \Input::get('email') : '';
        $tel        = \Input::has('tel') ? \Input::get('tel') : '';
        $line_id    = \Input::has('line_id') ? \Input::get('line_id') : '';

        // ตรวจสอบ username ว่ามีในระบบหรือยัง
        $chk_username   = User::where('username', $username)->count();
        if($chk_username > 1) return ['status' => 'error', 'msg' => 'Username is ready exits.'];

        // ตรวจสอบ email ว่ามีในระบบหรือยัง
        $chk_email  = User::where('email', $email)->count();
        if($chk_email > 1) return ['status' => 'error', 'msg' => 'Email is ready exits.'];

        // get data user เพื่อ update ข้อมูล
        $user   = User::where('id', $id)->first();

        
        if(empty($user)) return ['status' => 'error', 'msg' => 'Not found Data.'];

        // update ข้อมูล
        $user->firstname    = $firstname;
        $user->lastname     = $lastname;
        $user->username     = $username;
        if(!empty($password)){
            $user->password     = md5($password);
        }
        $user->email        = $email;
        $user->tel          = $tel;
        $user->line_id      = $line_id;
        $user->user_type    = 'user';
        $user->save();

        return ['status' => 'success'];
    }


    public function ajaxCenter(){
        
        $method   = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {
            case 'getDatafromSearch':
                
                $date   = \Input::has('date') ? \Input::get('date') : '';

                $reserve_1 = Reserve::where('reserve_date_chkin', $date)->where('rest_id', 1)->get();
                $reserve_2 = Reserve::where('reserve_date_chkin', $date)->where('rest_id', 2)->get();
                $reserve_3 = Reserve::where('reserve_date_chkin', $date)->where('rest_id', 3)->get();

                $count_1 = $reserve_1->count();
                $count_2 = $reserve_2->count();
                $count_3 = $reserve_3->count();

                $total_1 = 30 - $count_1;
                $total_2 = 5 - $count_2;
                $total_3 = 6 - $count_3;

                // d($reserve_1->count());
                // d($reserve_2->count());
                // sd($reserve_3->count());

                return ['status' => 'success', 'total_1' => $total_1, 'total_2' => $total_2, 'total_3' => $total_3, ];
                break;

            case 'getStatLoadpage':
                
                $date = date("l");

                // d(date("d"));
                // d(date("F"));
                // sd(date("Y"));

                $stat = Stat::where('day', date("d"));
                $stat = $stat->where('month', date("F"));
                $stat = $stat->where('year', date("Y"));
                $stat = $stat->first();


                if(!empty($stat)){
                    $stat->count = $stat->count+1;
                    $stat->save();
                }else{
                    $newstat            = new Stat;
                    $newstat->count     = 1;
                    $newstat->day       = date("d");
                    $newstat->month     = date("F");
                    $newstat->year      = date("Y");
                    $newstat->save();
                }
               

                return ['status' => 'success'];
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
        
    }


}