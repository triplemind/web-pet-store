<?php

namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;
// use App\Services\Test\Test;
use App\Services\Users\User;
use App\Services\Users\UserObject;
use App\Services\Users\UserRepository;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\Reserve\Reserve;
use App\Services\Rest\Rest;
use App\Services\Stat\Stat;

class ManageUserController extends Controller
{
	public function Index()
	{
		$users = User::where('id', '!=', 0)->paginate(30);

		return $this->view('admin.manage_user', compact('users'));
	}


    // edit data user
	public function postEdit(){

		$user_id 	= \Input::has('user_id') ? \Input::get('user_id') : '';
		$username 	= \Input::has('username') ? \Input::get('username') : '';
		$password 	= \Input::has('password') ? \Input::get('password') : '';
		$email 		= \Input::has('email') ? \Input::get('email') : '';
		$user_type 	= \Input::has('user_type') ? \Input::get('user_type') : '';
		$firstname 	= \Input::has('firstname') ? \Input::get('firstname') : '';
		$lastname 	= \Input::has('lastname') ? \Input::get('lastname') : '';
		$line_id 	= \Input::has('line_id') ? \Input::get('line_id') : '';
		$tel 		= \Input::has('tel') ? \Input::get('tel') : '';

		// ตรวจสอบ username ว่ามีในระบบหรือยัง
		$chk_username	= User::where('username', $username)->count();
		if($chk_username > 1) return helperReturnErrorFormRequestArray(['username' => 'Username is ready exits.']);

		// ตรวจสอบ email ว่ามีในระบบหรือยัง
		$chk_email	= User::where('email', $email)->count();
		if($chk_email > 1) return helperReturnErrorFormRequestArray(['email' => 'Email is ready exits.']);

		// get data user เพื่อ update ข้อมูล
		$user		= User::where('id', $user_id)->first();

		// check ว่ามี user id นี้ในระบบไหม
		if(empty($user)) return helperReturnErrorFormRequest('Not found Data.');

		// update ข้อมูล
		$user->firstname 	= $firstname;
		$user->lastname 	= $lastname;
		$user->username 	= $username;
		if ($password != '') $user->password 	= md5($password);
		$user->email 		= $email;
		$user->line_id 		= $line_id;
		$user->tel 			= $tel;
		$user->user_type 	= $user_type;
		$user->save();


		return ['status' => 'success'];
	}

	// remove user data
	public function postRemove(){

		$user_id  	= \Input::has('user_id') ? \Input::get('user_id') : '';
		
		// get data user เพื่อ remove ข้อมูล
		$user		= User::where('id', $user_id)->first();

		// check ว่ามี user id นี้ในระบบไหม
		if(empty($user)) return helperReturnErrorFormRequest('Not found Data.');

		// remove user data
		$user->delete();

		return ['status' => 'success'];

	}

	// เรียกใช้งาน form edit data
	public function ajaxCenter(){

		$method  	= \Input::has('method') ? \Input::get('method') : '';
		$user_repo 	= new UserRepository;

		// check case ว่าเรียก form จาก case อะไร
		switch ($method) {
			case 'getFormEditUser':
				
				$user_id 	= \Input::has('user_id') ? \Input::get('user_id') : '';

				// ตรวจสอบว่ามีการส่ง user id มาไหม
				if(empty($user_id)) return ['status' => 'error', 'msg' => 'field user_id is required.'];

				// get data user เพื่อ remove ข้อมูล
				$user 	= User::where('id', $user_id)->first();

				// check ว่ามี user id นี้ในระบบไหม
				if(empty($user)) return ['status' => 'error', 'msg' => 'field user_id is required.'];

				// ส่ง user data ไปแสดงใน form edit
				$form  	= $user_repo->genFormEditUser($user);

				return ['status' => 'success', 'form' => $form];
				break;

			default:
				return ['status' => 'error', 'msg' => 'Not found method'];
				break;
		}

	}


}