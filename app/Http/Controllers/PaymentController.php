<?php

namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;
// use App\Services\Test\Test;
use App\Services\Users\User;
use App\Services\Users\UserObject;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\Reserve\Reserve;
use App\Services\Rest\Rest;
use App\Services\Stat\Stat;

class PaymentController extends Controller
{
	public function Index(Request $request, $id)
	{
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        // มัดจำ 300 บาท only

		return $this->view('payment.index',compact('userObject','id'));
	}
    

    public function postAddPayment(){

    	$reserve_id  	= \Input::has('reserve_id') ? \Input::get('reserve_id') : '';
    	$payment_date  	= \Input::has('payment_date') ? \Input::get('payment_date') : '';
        $payment_time 	= \Input::has('payment_time') ? \Input::get('payment_time') : '';
        $cash        	= \Input::has('cash') ? \Input::get('cash') : '';
        $bank			= \Input::has('bank') ? \Input::get('bank') : '';
        $img_slip		= \Input::hasFile('img_slip') ? \Input::file('img_slip') : '';

        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        $reserve = Reserve::where('reserve_id',$reserve_id)->first();
        if(empty($reserve)) return helperReturnErrorFormRequest('Not found Data.');

        $payment = Payment::where('pay_id',$reserve->pay_id)->first();
        if(empty($payment)) return helperReturnErrorFormRequest('Not found Data.');

        $date = date('Y-m-d');
        $payment_slip = "";

        $path = base_path();
        if(!file_exists($path."/public/payment_slip_image")){
            $oldmask = umask(0);
            mkdir($path."/public/payment_slip_image", 0777);
            umask($oldmask);
        }

        if(!empty($img_slip)){
           
            $payment_slip = "/public/payment_slip_image/".$date."-".$img_slip->getClientOriginalName();
            copy($img_slip, $path.$payment_slip);
        }

        $payment->pay_img       = $payment_slip;
        $payment->pay_money     = $cash;
        $payment->pay_date      = $payment_date;
        $payment->pay_time      = $payment_time;
        $payment->pay_bank      = $bank;
        $payment->pay_number    = null;
        $payment->pay_status    = 'waiting';
        $payment->save();

    	return ['status' => 'success'];
    }

    public function ajaxCenter(){
        
    }


}