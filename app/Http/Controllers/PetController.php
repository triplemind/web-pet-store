<?php

namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;
// use App\Services\Test\Test;
use App\Services\Users\UserObject;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\Reserve\Reserve;
use App\Services\Rest\Rest;
use App\Services\Stat\Stat;

class PetController extends Controller
{
	public function Index()
	{
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        $getpets = Pet::where('user_id',$userObject->id)->get(); 
        // sd($getpets->toArray());

		return $this->view('pet.index',compact('userObject','getpets'));
	}

    public function getAdd()
    {
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        return $this->view('pet.add',compact('userObject'));
    }

    public function getEdit(Request $request, $id)
    {
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        $getpet = Pet::where('pet_id', $id)->first(); 

        return $this->view('pet.edit',compact('userObject','getpet'));
    }

    public function postAdd()
    {
        $pet_name  = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $pet_sex   = \Input::has('pet_sex') ? \Input::get('pet_sex') : '';
        $type_id   = \Input::has('type_id') ? \Input::get('type_id') : '';
        $pet_remark= \Input::has('pet_remark') ? \Input::get('pet_remark') : '';
        $pet_sick  = \Input::has('pet_sick') ? \Input::get('pet_sick') : '';
        $pet_img  = \Input::hasFile('pet_img') ? \Input::file('pet_img') : '';

        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        $date = date('Y-m-d');
        $pet_image_name = "";

        $path = base_path();
        if(!file_exists($path."/public/pet_image")){
            $oldmask = umask(0);
            mkdir($path."/public/pet_image", 0777);
            umask($oldmask);
        }

        if(!empty($pet_img)){
           
            $pet_image_name = "/public/pet_image/".$date."-".$pet_img->getClientOriginalName();
            copy($pet_img, $path.$pet_image_name);
        }

        // add ข้อมูล
        $pet              = new Pet;
        $pet->pet_name    = $pet_name;
        $pet->pet_sex     = $pet_sex;
        $pet->pet_img     = $pet_image_name;
        $pet->type_id     = $type_id;
        $pet->pet_remark  = $pet_remark;
        $pet->pet_sick    = $pet_sick;
        $pet->user_id     = $userObject->id;
        $pet->save();

        return ['status' => 'success'];
    }

    public function postEdit()
    {
        $pet_id    = \Input::has('pet_id') ? \Input::get('pet_id') : '';
        $pet_name  = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $pet_sex   = \Input::has('pet_sex') ? \Input::get('pet_sex') : '';
        $type_id   = \Input::has('type_id') ? \Input::get('type_id') : '';
        $pet_remark= \Input::has('pet_remark') ? \Input::get('pet_remark') : '';
        $pet_sick  = \Input::has('pet_sick') ? \Input::get('pet_sick') : '';
        $pet_img  = \Input::hasFile('pet_img') ? \Input::file('pet_img') : '';

        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        $pet  = Pet::where('pet_id', $pet_id)->first();

        if(empty($pet)) return helperReturnErrorFormRequest('Not found Data.');

        $date = date('Y-m-d');
        $pet_image_name = "";

        $path = base_path();
        if(!file_exists($path."/public/pet_image")){
            $oldmask = umask(0);
            mkdir($path."/public/pet_image", 0777);
            umask($oldmask);
        }

        if(!empty($pet_img)){
           
            $pet_image_name = "/public/pet_image/".$date."-".$pet_img->getClientOriginalName();
            copy($pet_img, $path.$pet_image_name);
        }

        $pet->pet_name    = $pet_name;
        $pet->pet_sex     = $pet_sex;
        $pet->type_id     = $type_id;
        if(!empty($pet_image_name)){
            $pet->pet_img     = $pet_image_name;
        }
        $pet->pet_remark  = $pet_remark;
        $pet->pet_sick    = $pet_sick;
        $pet->user_id     = $userObject->id;
        $pet->save();

        return ['status' => 'success'];
    }

    public function postRemove()
    {
        $pet_id    = \Input::has('pet_id') ? \Input::get('pet_id') : '';

        $pet  = Pet::where('pet_id', $pet_id)->first();

        $path = base_path();

        if(empty($pet)) return helperReturnErrorFormRequest('Not found Data.');

        if(!empty($pet->pet_img)){
            if(file_exists($path.$pet->pet_img)){
                unlink($path.$pet->pet_img);
            }
        }
        $pet->delete();

        return ['status' => 'success'];
    }


    public function ajaxCenter(){
        
    }


}