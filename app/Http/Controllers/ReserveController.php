<?php

namespace App\Http\Controllers;

// use App\Http\Requests;
use Illuminate\Http\Request;
// use App\Services\Test\Test;
use App\Services\Users\UserObject;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\Reserve\Reserve;
use App\Services\Rest\Rest;
use App\Services\Stat\Stat;

class ReserveController extends Controller
{
	public function Index()
	{
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';
        // sd($userObject);
        if(!empty($userObject)){
            $pets = Pet::where('user_id', $userObject->id)->get();
        }

        // sd($pets);

		return $this->view('reserve.index',compact('userObject','pets'));
	}

    public function getEdit(Request $request, $id)
    {
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';
        // sd($userObject);
        if(!empty($userObject)){

            $reserve = Reserve::with(['rest','user','payment'])->where('reserve_id', $id)->first();

            $pets = Pet::get();
        }

        // sd($reserve);

        return $this->view('reserve.edit',compact('userObject','pets','reserve'));
    }

    public function getReserveHistory()
    {
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';
        // sd($userObject);
        if(!empty($userObject)){
            $reserves = Reserve::with(['rest','user','payment'])->where('user_id', $userObject->id)->orderBy('created_at','DESC')->get();

            $pets = Pet::where('user_id', $userObject->id)->get();
        }

        // sd($reserves);

        return $this->view('reserve.reserve_history',compact('userObject','reserves','pets'));
    }

    public function getReserveDetail(Request $request, $id)
    {
        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';
        // sd($userObject);
        if(!empty($userObject)){
            $reserve = Reserve::with(['rest','user'])->where('reserve_id', $id)->first();

            $pets = Pet::where('user_id', $userObject->id)->get();

            $reserve_1 = Reserve::where('rest_id', $reserve->rest_id)->get();

            $count_1 = $reserve_1->count();

            if($reserve->rest_id == 1){
                $name_no = "CAGE_00".$count_1;
            }elseif ($reserve->rest_id == 2){
                $name_no = "STALL_00".$count_1;
            }elseif ($reserve->rest_id == 3) {
                $name_no = "FS_00".$count_1;
            }
            // sd($count_1);

        }

        return $this->view('reserve.reserve_detail',compact('userObject','reserve','pets','name_no'));
    }

    public function postAdd()
    {
        $reserve_date_chkin  = \Input::has('reserve_date_chkin') ? \Input::get('reserve_date_chkin') : '';
        $reserve_date_chkout = \Input::has('reserve_date_chkout') ? \Input::get('reserve_date_chkout') : '';
        $reserve_date        = \Input::has('reserve_date') ? \Input::get('reserve_date') : '';
        $rest_id             = \Input::has('rest_id') ? \Input::get('rest_id') : '';
        $count               = \Input::has('count') ? \Input::get('count') : '';
        $pet_name            = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $req_trans_type      = \Input::has('req_trans_type') ? \Input::get('req_trans_type') : '';

        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        // sd($pet_name);

        $payment                = new Payment;
        $payment->pay_img       = null;
        $payment->pay_money     = null;
        $payment->pay_date      = null;
        $payment->pay_time      = null;
        $payment->pay_bank      = null;
        $payment->pay_number    = null;
        $payment->pay_status    = 'pending';
        $payment->save();


        // add ข้อมูล
        $reserve                        = new Reserve;
        $reserve->reserve_date_chkin    = $reserve_date_chkin;
        $reserve->reserve_date_chkout   = $reserve_date_chkout;
        $reserve->reserve_date          = $reserve_date;
        $reserve->user_id               = $userObject->id;
        $reserve->pet_id                = json_encode($pet_name);
        $reserve->rest_id               = $rest_id;
        $reserve->req_trans_type        = $req_trans_type;
        $reserve->pet_status_update     = 'pending';
        $reserve->pet_return_status     = 'pending';
        $reserve->pay_id                 = $payment->pay_id;
        $reserve->save();

        return ['status' => 'success', "id" => $reserve->reserve_id];
    }

    public function postEdit()
    {
        $reserve_id          = \Input::has('reserve_id') ? \Input::get('reserve_id') : '';
        $reserve_date_chkin  = \Input::has('reserve_date_chkin') ? \Input::get('reserve_date_chkin') : '';
        $reserve_date_chkout = \Input::has('reserve_date_chkout') ? \Input::get('reserve_date_chkout') : '';
        $reserve_date        = \Input::has('reserve_date') ? \Input::get('reserve_date') : '';
        $rest_id             = \Input::has('rest_id') ? \Input::get('rest_id') : '';
        $count               = \Input::has('count') ? \Input::get('count') : '';
        $pet_name            = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $req_trans_type      = \Input::has('req_trans_type') ? \Input::get('req_trans_type') : '';

        $userObject = \Session::has('current_user') ? \Session::get('current_user') : '';

        $reserve = Reserve::where('reserve_id', $reserve_id)->first();

        if(empty($reserve)) return helperReturnErrorFormRequest('Not found Data.');

        // add ข้อมูล
        $reserve->reserve_date_chkin    = $reserve_date_chkin;
        $reserve->reserve_date_chkout   = $reserve_date_chkout;
        $reserve->reserve_date          = $reserve_date;
        // $reserve->user_id               = $userObject->id;
        if(!empty($pet_name)){
            $reserve->pet_id                = json_encode($pet_name);
        }
        $reserve->rest_id               = $rest_id;
        $reserve->req_trans_type        = $req_trans_type;
        $reserve->pet_status_update     = 'pending';
        $reserve->pet_return_status     = 'pending';
        $reserve->save();


        return ['status' => 'success', "id" => $reserve_id];
    }


    public function ajaxCenter(){
        
    }


}