<?php namespace App\Services\Auths;

use App\Services\Users\UserObject;
use App\Services\Users\User;

class UserAuth {


    public static function attempt($username, $password)
    {
        $userObject = new UserObject;

        // Query.
        $user   = User::where('username', $username)->where('password', $password);
        $user   = $user->orWhere(function($q) use ($username, $password) {
                        $q->where('email', $username)->where('password', $password);
                    });

        $userModel = $user->first(['id', 'firstname', 'lastname', 'username', 'email', 'line_id', 'user_type','tel']);


        if (empty($userModel)) return false;

        if($userModel->user_type == 'admin')  \session::put('ref_id', 0);
        // Save User to Session.
        $userObject->setUp($userModel);
        // Set Lang.
        // setLang();
        //Return.
        return $userModel;
	}

	public static function check()
	{
		$result = false;

        if (\Session::has('current_user')) {

        	$result = true;
        }

        return $result;
	}

    public static function check_user_type()
    {
        $result = false;

        if (\Session::has('current_user')) {
            if (\Session::get('current_user')->user_type == 'admin') {

                $result = true;
            }
        }

        return $result;
    }

    public static function clear()
    {
        \Session::forget('current_user');
        \Session::forget('ref_id');
        
    }
}