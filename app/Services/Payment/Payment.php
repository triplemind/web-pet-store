<?php namespace App\Services\Payment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'tbl_payment';
 	protected $primaryKey 	= 'pay_id';

}