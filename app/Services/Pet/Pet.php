<?php namespace App\Services\Pet;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'pet';
 	protected $primaryKey 	= 'pet_id';

}