<?php namespace App\Services\Reserve;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'tbl_reserve';
 	protected $primaryKey 	= 'reserve_id';

	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'user_id');
	}

	public function rest()
	{
		return $this->belongsTo("App\Services\Rest\Rest", 'rest_id');
	}

	public function payment()
	{
		return $this->belongsTo("App\Services\Payment\Payment", 'pay_id');
	}

}