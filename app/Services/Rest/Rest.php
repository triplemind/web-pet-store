<?php namespace App\Services\Rest;

use Illuminate\Database\Eloquent\Model;

class Rest extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'tbl_rest';
 	protected $primaryKey 	= 'rest_id';

}