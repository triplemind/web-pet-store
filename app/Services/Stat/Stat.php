<?php namespace App\Services\Stat;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'tbl_stats';
 	protected $primaryKey 	= 'id';

}