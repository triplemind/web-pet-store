<?php namespace App\Services\Users;

use App\Services\Users\User;

class UserObject {

	protected $user_id;
	protected $username;
	protected $user_type;
	protected $email;
	protected $user_img;

	public function __construct()
	{
		// Put Session to Object.
		if (\Session::has('current_user')) {

			$current_user 			= \Session::get('current_user');
			$this->id				= $current_user->id;
			$this->firstname 		= $current_user->firstname;
			$this->lastname 		= $current_user->lastname;
			$this->username 		= $current_user->username;
			$this->user_type 		= $current_user->user_type;
			$this->email 			= $current_user->email;
			$this->tel 				= $current_user->tel;
			$this->line_id 			= $current_user->line_id;
		}
	}

	public function setUp($userModel)
	{
		// Save to Session.
        \Session::put('current_user', $userModel);
    }

	public function getUser_id()
	{
		return $this->id;
	}
	public function getEmail()
	{
		return $this->email;
	}
	public function getFirstname()
	{
		return $this->firstname;
	}
	public function getLasrname()
	{
		return $this->lastname;
	}
	public function getUsername()
	{
		return $this->username;
	}
	public function getUserType()
	{
		return $this->user_type;
	}

	public function getTel()
	{
		return $this->tel;
	}

	public function getLine()
	{
		return $this->line_id;
	}


}