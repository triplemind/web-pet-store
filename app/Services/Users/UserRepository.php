<?php namespace App\Services\Users;

use App\Services\BaseRepository;

class UserRepository extends BaseRepository
{
	public function genFormEditUser($user)
	{
		$form = '';
		$form .= "<form class='form-horizontal' id='form-publizh-product'>";
			$form .= "<div class='form-group' style='margin-bottom:30px;margin-top:30px;'>";
				$form .= "<div class='row' style='text-align:center;color:black;'>";
					$form .= "<i class='fa fa-pencil-square-o' style='font-size:60px;color:#ff9800'></i>";
					$form .= "<h3 > EDIT USER</h3>";
					$form .= "<input type='hidden' name='user_id' value='".$user->id."' />";
				$form .= "</div>";
			$form .= "</div>";

			$form .= "<div class='form-group'>";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>Firstname :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<input type='text' class='form-control' name='firstname' value='".$user->firstname."' placeholder='' />";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-firstname'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

			$form .= "<div class='form-group'>";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>Lastname :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<input type='text' class='form-control' name='lastname' value='".$user->lastname."' placeholder='' />";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-lastname'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

			$form .= "<div class='form-group' >";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>Username :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<input type='text' class='form-control inp-require' name='username' value='".$user->username."' placeholder='' />";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-username'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

			$form .= "<div class='form-group'>";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>Email :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<input type='text' class='form-control inp-require' name='email' value='".$user->email."' placeholder='' />";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-email'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

			$form .= "<div class='form-group'>";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>Password :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<input type='password' class='form-control inp-require' name='password' placeholder='' />";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-password'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

			$form .= "<div class='form-group'>";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>User Type :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<select name='user_type' class='form-control inp-require' placeholder=''>";
								$form .= "<option value='admin'".($user->user_type == 'admin' ? 'selected' : '').">Admin</option>";
								$form .= "<option value='user' ".($user->user_type == 'user' ? 'selected' : '').">User</option>";
								$form .= "</select>";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-user_type'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

			$form .= "<div class='form-group'>";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>Telephone :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<input type='text' class='form-control inp-require' name='tel' value='".$user->tel."' placeholder='' />";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-tel'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

			$form .= "<div class='form-group'>";
				$form .= "<div class='row'>";
					$form .= "<div class='col-sm-3 text-right'>";
						$form .= "<label>Line ID :</label>";
					$form .= "</div>";
					$form .= "<div class='col-sm-8'>";
						$form .= "<div class='form-group'>";
		                    $form .= "<div class='form-line'>";
								$form .= "<input type='text' class='form-control inp-require' name='line_id' value='".$user->line_id."' placeholder='' />";
		                    $form .= "</div>";
							$form .= "<div class='text-error text-error-tel'></div>";
		                $form .= "</div>";
					$form .= "</div>";
					$form .= "<div class='col-sm-1'></div>";
				$form .= "</div>";
			$form .= '</div>';

		$form .= '</form>';

		return $form;
	}
}