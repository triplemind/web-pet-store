<style type="text/css">
    #shiva
    {
        width: 100%;
        height: 60px;
        background: #00BCD4;
        border-radius: 14px;
    }

    #getstat
    {
        width: 100%;
        height: 100px;
        background: #cddc39;
        border-radius: 14px;
    }

    .count
    {
        line-height: 10px;
        color:white;
        /*margin-left:30px;*/
        font-size:25px;
    }
    .btn-warning:hover{
        border-color: #ffa72b;
        background-color: #ffa72b;
    }
    .btn-warning:focus{
        border-color: #ffa72b;
        background-color: #ffa72b;
    }

    .btn-danger:hover{
        border-color: #e03832;
        background-color: #e03832;
    }

    .btn-danger:focus{
        border-color: #e03832;
        background-color: #e03832;
    }
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="hero-content text-center " style="padding-top: 15%;">
            <!-- <img src="/themes/img/New Project.png" style="width: 70%"> -->
            <div class="row" style="background: #ffffffd6;padding: 2em;">
                <div class="col-md-12" style="padding-bottom: 2em;">
                    <h3 style="padding: 0em;">
                        <span><img src="/themes/img/icons8-statistics-64.png"></span>
                        จำนวนคนเข้าชมเว็บไซต์
                    </h3>

                    <div class="col-md-4" style="padding: 1em;">
                        <div id="getstat">
                            <h4 style="padding: 1em;">วันนี้</h4>
                            <span class="count"><?php echo $getstatDay->day_count; ?></span>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding: 1em;">
                        <div id="getstat">
                            <h4 style="padding: 1em;">ประจำเดือนนี้</h4>
                            <span class="count"><?php echo $getstatMonth->month_count; ?></span>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding: 1em;">
                        <div id="getstat">
                            <h4 style="padding: 1em;">ประจำปีนี้</h4>
                            <span class="count"><?php echo $getstatYear->year_count; ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="border-top: 2px solid #7b7b7b;padding-top: 1em;">
                    <div class="col-md-6">
                        <div class="sign-up">
                            <div class="col-12 signup-form" style="background-color: #ffffff00;">
                                <h2 style="background-color: #bdbdbd;">ตรวจสอบวันว่าง</h2>
                                <div style="padding: 35px 0px;">
                                    <div class="form-input-group" style="width: 45%; height: 45px;">
                                        <i class="fa fa-calendar"></i><input type="text" class="date datetimepicker" placeholder="Enter your Birthday" name="birthday" value="">
                                    </div>
                                </div>

                                <div style="padding-bottom: 30px;">
                                    <button type="submit" class="btn-fill btn-small btn-sarch">ค้นหา</button>
                                    <?php if(!empty($userObject)): ?>
                                    <button type="submit" class="btn-fill btn-small btn-booking">จอง</button>
                                    <?php endif ?>
                                </div> 

                                <?php if($getpets->count() == 0): ?>
                                    <input type="hidden" name="pet_count" value="<?php echo $getpets->count(); ?>">
                                    <div style="color: red; padding: 10px">
                                        *** กรุณากรองข้อมูลสัตว์เลี้ยงก่อนทำการจอง ***
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12" style="padding: 1em;">
                            <div id="shiva">
                                <h4 class="col-md-4" style="padding: 10px;color: #fff;">กรง</h4>
                                <span class="col-md-8 count" style="line-height: 55px;"><?php echo $total_1; ?></span>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 1em;">
                            <div id="shiva">
                                <h4 class="col-md-4" style="padding: 10px;color: #fff;">คอก</h4>
                                <span class="col-md-8 count" style="line-height: 55px;"><?php echo $total_2; ?></span>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 1em;">
                            <div id="shiva">
                                <h4 class="col-md-4" style="padding: 10px;color: #fff;">พื้นที่ว่าง</h4>
                                <span class="col-md-8 count" style="line-height: 55px;"><?php echo $total_3; ?></span>
                            </div>
                        </div>

                        <div class="col-md-12" style="color: red; padding: 10px">
                            *** จำนวนกรงทั้งหมด 30, จำนวนคอกทั้งหมด 5, จำนวนพื้นที่วาง กรง/คอก ส่วนตัวทั้งหมด 6 ***
                        </div>
                    </div>
                </div>


                <div class="col-12">
                <div class="col-md-12" style="border-top: 2px solid #7b7b7b;padding-top: 2em;margin-top: 3em;">
                    <h3 style="padding: 0em;">
                        <span><img src="/themes/img/icons8-reserve-80.png"></span>
                        การจองล่าสุด
                    </h3>
                    <table class="table table-striped table-hover js-basic-example dataTable table-responsive" style="background-color: #dedfee;">
                        <thead>
                            <th style="text-align: center;">#</th>
                            <th style="text-align: center;">ชื่อผู้ใช้งาน</th>
                            <th style="text-align: center;">Check-in</th>
                            <th style="text-align: center;">Check-out</th>
                            <th style="text-align: center;">สถานะการชำระเงิน</th>
                            <th style="text-align: center;">สถานะการรับสัตว์เลี้ยง</th>
                            <th style="text-align: center;">สถานะการส่งคืนสัตว์เลี้ยง</th>
                            <th style="text-align: center;"></th>
                        </thead>
                        <tbody>
                            <!-- get data จากตาราง payment มาแสดง โดยใช้ loop for เพื่อชี้ข้อมูลแต่ละ field -->
                            <?php if ($getlastbooking->count() !== 0):?>
                                <?php foreach ($getlastbooking as $key => $getlast):?>
                                    <tr>
                                        <td><?php echo $key+1 ?></td>
                                        <td><?php echo empty($getlast->user) ? '' : $getlast->user->firstname.' '.$getlast->user->lastname ?></td>
                                        <td><?php echo $getlast->reserve_date_chkin ?></td>
                                        <td><?php echo $getlast->reserve_date_chkout ?></td>
                                        <td>
                                            <?php 
                                                $status = "";

                                                if(!empty($getlast->payment)){

                                                    if($getlast->payment->pay_status == "pending"){
                                                        $status = "รอดำเนินการ";
                                                    }elseif($getlast->payment->pay_status == "waiting"){
                                                        $status = "เจ้าหน้าที่กำลังตรวจสอบ";
                                                    }elseif($getlast->payment->pay_status == "completed"){
                                                        $status = "ดำเนินการเสร็จสิ้น";
                                                    }
                                                }

                                                echo $status;
                                            ?>
                                        </td>
                                        <td>
                                            <?php 

                                                $pet_status = "";

                                                if($getlast->pet_status_update == "pending"){
                                                    $pet_status = "ยังไม่ได้รับ";
                                                }elseif($getlast->pet_status_update == "completed"){
                                                    $pet_status = "ได้รับแล้ว";
                                                }

                                                $status         = strtolower($pet_status);
                                                $select_status  = ($status == 'ได้รับแล้ว' ? "checked" : '');
                                                $clss_status    = ($status == 'ได้รับแล้ว' ? "label-info" : 'label-danger');
                                            ?>
                                            <span class="label <?php echo $clss_status; ?>" style="font-size: 12px;margin-bottom: 5px;"><?php echo $status; ?></span>

                                            <label class="switch">
                                                <input class="on-off-pet-come" type="checkbox" <?php echo $select_status; ?> value="<?php echo $getlast->reserve_id; ?>">
                                                <span class="slider round"></span>
                                            </label>
                                                
                                        </td>
                                        <td>
                                            <?php 

                                                $pet_status = "";

                                                if($getlast->pet_return_status == "pending"){
                                                    $pet_status = "ยังไม่ได้ส่งคืน";
                                                }elseif($getlast->pet_return_status == "completed"){
                                                    $pet_status = "ส่งคืนแล้ว";
                                                }

                                                $status         = strtolower($pet_status);
                                                $select_status  = ($status == 'ส่งคืนแล้ว' ? "checked" : '');
                                                $clss_status    = ($status == 'ส่งคืนแล้ว' ? "label-info" : 'label-danger');
                                            ?>
                                            <span class="label <?php echo $clss_status; ?>" style="font-size: 12px;margin-bottom: 5px;"><?php echo $status; ?></span>

                                            <label class="switch">
                                                <input class="on-off-pet-return" type="checkbox" <?php echo $select_status; ?> value="<?php echo $getlast->reserve_id; ?>">
                                                <span class="slider round"></span>
                                            </label>
                                                
                                        </td>
                                        <td width="20%">
                                            <div class="row text-center">
                                                <button class=" btn btn-warning btn-edit" data-reserve_id="<?php echo $getlast->reserve_id; ?>">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    <!-- <span>แก้ไข</span> -->
                                                </button>
                                                <?php if($getlast->payment->pay_status !== "completed"): ?>
                                                    <button class="btn btn-fill btn-payment" data-reserve_id="<?php echo $getlast->reserve_id; ?>">
                                                            <!-- <i class="fa fa-trash-o"></i> -->
                                                        <span>ตรวจสอบ</span>
                                                    </button>
                                                <?php endif ?>
                                                <button class="btn btn-danger btn-delete" data-reserve_id="<?php echo $getlast->reserve_id; ?>">
                                                        <i class="fa fa-trash-o"></i>
                                                    <!-- <span>ลบ</span> -->
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tbody>
                    </table>
                    <!-- แสดงตัวเลข page -->
                    <?php //echo $payments->render(); ?>
                    
                </div>
            </div>

            </div>
        </div>
    </div>
</div>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="ajax_center_url" data-url="<?php echo \URL::route('main.ajax_center.post'); ?>"></div>
<div id='delete-url' data-url="<?php echo \URL::route('detail.remove.post');?>"></div>


<script>
	$(function(){

        // CALL Data Table
        $('.dataTables_length').css('display', 'none');
        $('.dataTables_paginate').css('display', 'none');

        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });


		$('.datetimepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose : true,
        });

		$('.btn-booking').on('click', function(){ 
            var pet_count = $('input[name=pet_count]').val();

            if(pet_count == 0){
                swal({
                    title:'คุณยังไม่มีข้อมูลสัตว์เลี้ยงในระบบ', 
                    text: 'กรุณากรอกข้อมูลสัตว์เลี้ยงก่อนทำรายการ', 
                    type:'error', 
                    confirmButtonText:  'OK'},
                    function(){
                      window.location.href = "/my_pet";
                    });
            }else{
                window.location.href = "/reserve";
            }
        });


        $('.btn-sarch').on('click', function(){
            msg_waiting();
            var date                = $('.datetimepicker').val();
            var method              = 'getDatafromSearch';
            var ajax_center_url     = $('#ajax_center_url').data('url');

            if(date !== ""){
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: "POST",
                    url: ajax_center_url,
                    data: {
                        method : method,
                        date : date,

                    },
                    success: function(Response) {
                        console.log(Response.status);
                        console.log(Response.total_1);
                        console.log(Response.total_2);
                        console.log(Response.total_3);

                        if(Response.status == "success"){
                            // msg_stop();
                            var message = '<div><h4> วันที่ : '+date+'</h4>'
                                    +'<h3>จำนวนที่ว่าง</h3><h4 style="color: #ad0a0a;">'+Response.total_1+'</h4>'
                                    +'<div style="color: red;font-size:15px;">*** จำนวนพื้นที่วาง กรง/คอก ทั้งหมด 30 ที่ ***</div>'
                                    +'<hr style="border-top: 1px solid #bbb;">'
                                    +'<h3>จำนวนกรงที่ว่าง</h3>'
                                    +'<h4 style="color: #ad0a0a;">'+Response.total_2+'</h4>'
                                    +'<div style="color: red;font-size:15px;">*** จำนวนกรงทั้งหมด 5 ที่ ***</div>'
                                    +'<hr style="border-top: 1px solid #bbb;">'
                                    +'<h3>จำนวนคอกที่ว่าง</h3>'
                                    +'<h4 style="color: #ad0a0a;">'+Response.total_3+'</h4></div>'
                                    +'<div style="color: red;font-size:15px;">*** จำนวนคอกทั้งหมด 6 ที่ ***</div>';


                            swal({title: "ตรวจสอบวันว่าง", html: true, text:message});
                        }

                        if(Response.status == "error"){

                        }

                    }
                });

            }else{

                msg_unsuccess('กรุณาระบุวันที่ต้องการค้นหา !!');

            }
            
        });


        $('.btn-edit').on('click', function(){
        window.location.href = "/reserve/edit/"+$(this).data('reserve_id');
    });

    $('.btn-payment').on('click', function(){
        window.location.href = "/admin/payment_detail/"+$(this).data('reserve_id');
    });


    $('.js-basic-example').on('change', '.on-off-pet-come', function(){
        var pet_come_obj        = $(this);
        var pet_come_id         = $(this).val();
        var status_change       = $(this).is(":checked");

        var ajax_url        = $('#ajax-center-url').data('url');
        var method          = 'pendingOrCompletePetCome';
        swal({
            title: "Are you sure?",
            text: "You will update status this Reserve?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function(isConfirm) {
            if (isConfirm) {
                msg_waiting();
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: 'post',
                    url: ajax_url,
                    data: {
                        'method' : method,
                        'pet_come_id' : pet_come_id,
                        'status_change' : status_change,
                    },
                    success: function(result) {
                        if (result.status === 'success')
                            window.location.reload();
                    },
                });
            }else{
                if(status_change){
                    pet_come_obj.removeAttr('checked');
                }else{
                    pet_come_obj.click();
                }
            }
        });
    });



    $('.js-basic-example').on('change', '.on-off-pet-return', function(){
        var pet_return_obj        = $(this);
        var pet_return_id         = $(this).val();
        var status_change       = $(this).is(":checked");

        var ajax_url        = $('#ajax-center-url').data('url');
        var method          = 'pendingOrCompletePetReturn';
        swal({
            title: "Are you sure?",
            text: "You will update status this Reserve?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function(isConfirm) {
            if (isConfirm) {
                msg_waiting();
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: 'post',
                    url: ajax_url,
                    data: {
                        'method' : method,
                        'pet_return_id' : pet_return_id,
                        'status_change' : status_change,
                    },
                    success: function(result) {
                        if (result.status === 'success')
                            window.location.reload();
                    },
                });
            }else{
                if(status_change){
                    pet_return_obj.removeAttr('checked');
                }else{
                    pet_return_obj.click();
                }
            }
        });
    });



     // remove data บัตรรายครั้ง
    $('.js-basic-example').on('click', '.btn-delete', function(){

        var remove_id = $(this).data('reserve_id');

        swal({
                title: "Are you sure?",
                text: "Do you want delete this reserve ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    msg_waiting();
                    postRemoveReserve(remove_id);
                }
         });
    });

	});


function postRemoveReserve(remove_id){
    var data        = new FormData();
    var delete_url  = $('#delete-url').data('url');
   
    data.append('remove_id', remove_id);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: delete_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                window.location.reload();
            } // End if check s tatus success.

            if(result.status == 'error'){
                
            }
        },
        error : function(error) {
            console.log(error);
        }
    });

}

</script>