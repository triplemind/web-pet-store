<div class="row">
    <div class="col-md-12">
        <div class="hero-content text-center ">
            
            <!-- <h1 class="text-center" style="color: #000;font-weight: bold;">จัดการผู้ใช้งาน</h1> -->
            <div style="color: #000;text-align: left;border-bottom: solid 1px #000;font-size: 22px;margin-bottom: 20px;">รายละอียดการจอง</div>

            <div class="col-12">
                <div class="col-md-12" style="margin-bottom: 20%;">
                    <table class="table table-striped table-hover js-basic-example dataTable table-responsive" style="background-color: #dedfee;">
                        <thead>
                            <th style="text-align: center;">#</th>
                            <th style="text-align: center;">ชื่อผู้ใช้งาน</th>
                            <th style="text-align: center;">Check-in</th>
                            <th style="text-align: center;">Check-out</th>
                            <th style="text-align: center;">สถานะการชำระเงิน</th>
                            <th style="text-align: center;">สถานะการรับสัตว์เลี้ยง</th>
                            <th style="text-align: center;">สถานะการส่งคืนสัตว์เลี้ยง</th>
                            <th style="text-align: center;"></th>
                        </thead>
                        <tbody>
                            <!-- get data จากตาราง payment มาแสดง โดยใช้ loop for เพื่อชี้ข้อมูลแต่ละ field -->
                            <?php if ($payments->count() !== 0):?>
                                <?php foreach ($payments as $key => $payment):?>
                                    <tr>
                                        <td><?php echo $key+1 ?></td>
                                        <td><?php echo empty($payment->user) ? '' : $payment->user->firstname.' '.$payment->user->lastname ?></td>
                                        <td><?php echo $payment->reserve_date_chkin ?></td>
                                        <td><?php echo $payment->reserve_date_chkout ?></td>
                                        <td>
                                            <?php 
                                                $status = "";

                                                if(!empty($payment->payment)){

                                                    if($payment->payment->pay_status == "pending"){
                                                        $status = "รอดำเนินการ";
                                                    }elseif($payment->payment->pay_status == "waiting"){
                                                        $status = "เจ้าหน้าที่กำลังตรวจสอบ";
                                                    }elseif($payment->payment->pay_status == "completed"){
                                                        $status = "ดำเนินการเสร็จสิ้น";
                                                    }
                                                }

                                                echo $status;
                                            ?>
                                        </td>
                                        <td>
                                            <?php 

                                                $pet_status = "";

                                                if($payment->pet_status_update == "pending"){
                                                    $pet_status = "ยังไม่ได้รับ";
                                                }elseif($payment->pet_status_update == "completed"){
                                                    $pet_status = "ได้รับแล้ว";
                                                }

                                                $status         = strtolower($pet_status);
                                                $select_status  = ($status == 'ได้รับแล้ว' ? "checked" : '');
                                                $clss_status    = ($status == 'ได้รับแล้ว' ? "label-info" : 'label-danger');
                                            ?>
                                            <span class="label <?php echo $clss_status; ?>" style="font-size: 12px;margin-bottom: 5px;"><?php echo $status; ?></span>

                                            <label class="switch">
                                                <input class="on-off-pet-come" type="checkbox" <?php echo $select_status; ?> value="<?php echo $payment->reserve_id; ?>">
                                                <span class="slider round"></span>
                                            </label>
                                                
                                        </td>
                                        <td>
                                            <?php 

                                                $pet_status = "";

                                                if($payment->pet_return_status == "pending"){
                                                    $pet_status = "ยังไม่ได้ส่งคืน";
                                                }elseif($payment->pet_return_status == "completed"){
                                                    $pet_status = "ส่งคืนแล้ว";
                                                }

                                                $status         = strtolower($pet_status);
                                                $select_status  = ($status == 'ส่งคืนแล้ว' ? "checked" : '');
                                                $clss_status    = ($status == 'ส่งคืนแล้ว' ? "label-info" : 'label-danger');
                                            ?>
                                            <span class="label <?php echo $clss_status; ?>" style="font-size: 12px;margin-bottom: 5px;"><?php echo $status; ?></span>

                                            <label class="switch">
                                                <input class="on-off-pet-return" type="checkbox" <?php echo $select_status; ?> value="<?php echo $payment->reserve_id; ?>">
                                                <span class="slider round"></span>
                                            </label>
                                                
                                        </td>
                                        <td width="20%">
                                            <div class="row text-center">
                                                <button class=" btn btn-warning btn-edit" data-reserve_id="<?php echo $payment->reserve_id; ?>">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    <!-- <span>แก้ไข</span> -->
                                                </button>
                                                <?php if($payment->payment->pay_status !== "completed"): ?>
                                                    <button class="btn btn-fill btn-payment" data-reserve_id="<?php echo $payment->reserve_id; ?>">
                                                            <!-- <i class="fa fa-trash-o"></i> -->
                                                        <span>ตรวจสอบ</span>
                                                    </button>
                                                <?php endif ?>
                                                <button class="btn btn-danger btn-delete" data-reserve_id="<?php echo $payment->reserve_id; ?>">
                                                        <i class="fa fa-trash-o"></i>
                                                    <!-- <span>ลบ</span> -->
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tbody>
                    </table>
                    <!-- แสดงตัวเลข page -->
                    <?php //echo $payments->render(); ?>
                    
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    .btn-warning:hover{
        border-color: #ffa72b;
        background-color: #ffa72b;
    }
    .btn-warning:focus{
        border-color: #ffa72b;
        background-color: #ffa72b;
    }

    .btn-danger:hover{
        border-color: #e03832;
        background-color: #e03832;
    }

    .btn-danger:focus{
        border-color: #e03832;
        background-color: #e03832;
    }
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>


<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('admin.ajax_center.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('detail.remove.post');?>"></div>
   



<script>
    $(function(){

    // CALL Data Table
    $('.dataTables_length').css('display', 'none');
    $('.dataTables_paginate').css('display', 'none');

    $('.btn-edit').on('click', function(){
        window.location.href = "/reserve/edit/"+$(this).data('reserve_id');
    });

    $('.btn-payment').on('click', function(){
        window.location.href = "/admin/payment_detail/"+$(this).data('reserve_id');
    });


    $('.js-basic-example').on('change', '.on-off-pet-come', function(){
        var pet_come_obj        = $(this);
        var pet_come_id         = $(this).val();
        var status_change       = $(this).is(":checked");

        var ajax_url        = $('#ajax-center-url').data('url');
        var method          = 'pendingOrCompletePetCome';
        swal({
            title: "Are you sure?",
            text: "You will update status this Reserve?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function(isConfirm) {
            if (isConfirm) {
                msg_waiting();
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: 'post',
                    url: ajax_url,
                    data: {
                        'method' : method,
                        'pet_come_id' : pet_come_id,
                        'status_change' : status_change,
                    },
                    success: function(result) {
                        if (result.status === 'success')
                            window.location.reload();
                    },
                });
            }else{
                if(status_change){
                    pet_come_obj.removeAttr('checked');
                }else{
                    pet_come_obj.click();
                }
            }
        });
    });



    $('.js-basic-example').on('change', '.on-off-pet-return', function(){
        var pet_return_obj        = $(this);
        var pet_return_id         = $(this).val();
        var status_change       = $(this).is(":checked");

        var ajax_url        = $('#ajax-center-url').data('url');
        var method          = 'pendingOrCompletePetReturn';
        swal({
            title: "Are you sure?",
            text: "You will update status this Reserve?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function(isConfirm) {
            if (isConfirm) {
                msg_waiting();
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: 'post',
                    url: ajax_url,
                    data: {
                        'method' : method,
                        'pet_return_id' : pet_return_id,
                        'status_change' : status_change,
                    },
                    success: function(result) {
                        if (result.status === 'success')
                            window.location.reload();
                    },
                });
            }else{
                if(status_change){
                    pet_return_obj.removeAttr('checked');
                }else{
                    pet_return_obj.click();
                }
            }
        });
    });



     // remove data บัตรรายครั้ง
    $('.js-basic-example').on('click', '.btn-delete', function(){

        var remove_id = $(this).data('reserve_id');

        swal({
                title: "Are you sure?",
                text: "Do you want delete this reserve ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    msg_waiting();
                    postRemoveReserve(remove_id);
                }
         });
    });


});

function postRemoveReserve(remove_id){
    var data        = new FormData();
    var delete_url  = $('#delete-url').data('url');
   
    data.append('remove_id', remove_id);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: delete_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                window.location.reload();
            } // End if check s tatus success.

            if(result.status == 'error'){
                
            }
        },
        error : function(error) {
            console.log(error);
        }
    });

}


</script>