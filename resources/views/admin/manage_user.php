<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <!-- <h1 class="text-center" style="color: #000;font-weight: bold;">จัดการผู้ใช้งาน</h1> -->
            <div style="color: #000;text-align: left;border-bottom: solid 1px #000;font-size: 22px;margin-bottom: 20px;">จัดการข้อมูลผู้ใช้งาน</div>

            <div class="col-12">
                <div class="col-md-12" style="margin-bottom: 20%;">
                    <table class="table table-striped table-hover js-basic-example dataTable table-responsive" style="background-color: #dedfee;">
                        <thead>
                            <th>#</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Line ID</th>
                            <th>Email</th>
                            <th>Telephone</th>
                            <th>User Type</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <!-- get data จากตาราง user มาแสดง โดยใช้ loop for เพื่อชี้ข้อมูลแต่ละ field -->
                            <?php foreach ($users as $key => $user):?>
                                <tr>
                                    <td><?php echo $key+1 ?></td>
                                    <td><?php echo $user->firstname ." ". $user->lastname ?></td>
                                    <td><?php echo $user->username ?></td>
                                    <td><?php echo $user->line_id ?></td>
                                    <td><?php echo $user->email ?></td>
                                    <td><?php echo $user->tel ?></td>
                                    <td><?php echo $user->user_type ?></td>
                                    <td width="15%">
                                        <div class="row text-center">
                                            <button class=" btn btn-warning btn-edit-user"  data-user_id="<?php echo $user->id; ?>">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                <!-- <span>Edit</span> -->
                                            </button>
                                            <button class="btn btn-danger btn-delete-user" data-username="<?php echo $user->username; ?>" data-user_id="<?php echo $user->id; ?>">
                                                    <i class="fa fa-trash-o"></i>
                                                <!-- <span>Delete</span> -->
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>

                        </tbody>
                    </table>
                    <!-- แสดงตัวเลข page -->
                    <?php echo $users->render(); ?>
                    
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('manage_user.ajax_center.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('manage_user.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('manage_user.delete.post');?>"></div>



<script>
    $(function(){

    // CALL Data Table
    $('.dataTables_length').css('display', 'none');
    $('.dataTables_paginate').css('display', 'none');

    // 

    // EDIT USER
    $('.js-basic-example').on('click', '.btn-edit-user', function(){
        // popup waiting
        msg_waiting();
        getFormEditUser($(this).data('user_id'));
    });

    // DELETE USER
    $('.js-basic-example').on('click', '.btn-delete-user', function(){
        var user_id = $(this).data('user_id');
        swal({
              title: "Are you sure?",
              text: "Do you want delete username => "+$(this).data('username')+" ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes",
              cancelButtonText: "No",
              closeOnConfirm: true,
              closeOnCancel: true
            },
              function(isConfirm) {
              if (isConfirm) {
                   msg_waiting();
                postRemove(user_id);

              }
         });
    });



});


// เรียกใช้ form edit user
function getFormEditUser(user_id)
{
    var method      = 'getFormEditUser';
    var ajax_url    = $('#ajax-center-url').data('url');
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'user_id' : user_id,
        },
        success: function(result) {
            if(result.status == 'success'){
                msg_stop();
                // เรียกใช้ function เพื่อแสดง form edit
                showForm(result.form, 'edit');
            } // End if check s tatus success.
        }
    });

}


// function แสดง form edit
function showForm(form, action, validation = '', data = '')
{
    var box = bootbox.dialog({
        className: 'bootbox-show-detail-magazine',
        message : form,
        backdrop: true,
        closeButton: false,
        size: 'large',
        buttons : {
            danger: {
                label: 'close',
                className: 'btn-default',
                callback: function() {
                    box.modal("hide");
                }
            },
            success: {
                label: 'Save',
                className: 'btn-primary',
                callback: function() {
                 if(action == 'edit')   postEdit(form, action);

                }
            }
        },
    });
     box.on('shown.bs.modal', function(){
            // $.AdminBSB.input.activate();
            // $.AdminBSB.select.activate();

        // เรียกใช้งาน function datepicker สำหรับแสดงปฏิทิน
        $('.datetimepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose : true,
        });

        
    });
}


function postEdit(form, action)
{
    var user_id         = $('input[name=user_id]').val();
    var username        = $('input[name=username]').val();
    var email           = $('input[name=email]').val();
    var password        = $('input[name=password]').val();
    var user_type       = $('select[name=user_type]').val();
    var firstname       = $('input[name=firstname]').val();
    var lastname        = $('input[name=lastname]').val();
    var tel             = $('input[name=tel]').val();
    var line_id         = $('input[name=line_id]').val();

    var edit_url        = $('#edit-url').data('url');
    var data            = new FormData();

    data.append('user_id', user_id);
    data.append('username', username);
    data.append('email',email);
    data.append('password',password);
    data.append('user_type',user_type);
    data.append('firstname',firstname);
    data.append('lastname',lastname);
    data.append('line_id',line_id);
    data.append('tel',tel);

    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    if (testEmail.test(email)){
        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: 'post',
            url: edit_url,
            data: data,
            contentType: false,
            processData:false,
            cache: false,
            success: function(result) {
                if(result.status == 'success'){
                    window.location.reload();
                } // End if check s tatus success.

                if(result.status == 'error'){
                    
                }
            },
            error : function(error) {
                console.log(error);
                showForm(form, action, error, data);
            }
        });
    }else{
        msg_error_custom("Invalid Email", "Please check your email format.");
    }


}


function postRemove(user_id)
{
    var delete_url      = $('#delete-url').data('url');

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: delete_url,
        data: {
             'user_id' : user_id,
        },
        success: function(result) {
            if(result.status == 'success'){
                window.location.reload();
            } // End if check s tatus success.
        },
        error : function(error) {
            msg_error_custom('Error!', error);
        }
    });
}
</script>