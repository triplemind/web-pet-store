
<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-user {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-location-arrow{
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-phone {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-envelope-o {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-car {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-calendar {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
</style>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="" style="background-color: rgba(52, 50, 50, 0.61);border: 2px solid #14d21c;border-radius: 55px;">
                <div class="row signup-form" >
                    <h2 style="color: #fff">รายละเอียดการจอง</h2>
                    <!--  col-md-offset-3 -->
                    <div class="col-md-5" style="margin-bottom: 30px;">
                    	<div class="row" style="text-align: center;">

	                        <div class="col-md-12">
		                        <div class="col-md-6" style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">รหัสการจอง : </div>
		                        <div class="col-md-6" style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo "00".$reserve->reserve_id; ?></div>
		                    </div>

		                    <div class="col-md-12">
	                            <div class="col-md-6" style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">รหัส กรง, คอก, พื้นที่วาง กรง/คอก ส่วนตัว : </div>
	                            <div class="col-md-6" style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo $name_no; ?></div>
	                        </div>

	                        <div class="col-md-12">
		                        <div class="col-md-6" style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">รหัสสมาชิก : </div>
								<div class="col-md-6" style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo "00".$reserve->user_id; ?></div>
							</div>


	                        <div class="col-md-12">
		                        <div class="col-md-6" style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">ชื่อสัตว์เลี้ยง : </div>
	                            <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;">	 <?php 
		                                $pet_id = json_decode($reserve->pet_id,true);

		                                foreach ($pets as $key => $pet){
		                                    if(in_array($pet->pet_id, $pet_id)){
		                                        echo $pet->pet_name.", ";
		                                    }
		                                }
		                            ?>
		                        </div>
		                    </div>


	                        <div class="col-md-12">
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">ชื่อ-สกุล :</div>
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo !empty($reserve->user) ? $reserve->user->firstname." ".$reserve->user->lastname : ""; ?></div>
		                    </div>


	                        <div class="col-md-12">
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">Check-in : </div>
		                        <div style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo $reserve->reserve_date_chkin; ?></div>
		                    </div>


	                        <div class="col-md-12">
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">Check-out : </div>
		                        <div style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo $reserve->reserve_date_chkout; ?></div>
		                    </div>


	                        <div class="col-md-12">
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">เวลานำสัตว์เข้าฝาก : </div>
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo $reserve->reserve_date; ?></div>
		                    </div>


	                        <div class="col-md-12">
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">ประเภทที่พัก : </div>
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo !empty($reserve->rest) ? $reserve->rest->rest_name : ""; ?></div>
		                    </div>

	                        <div class="col-md-12">
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">บริการรับ-ส่ง :</div>
		                        <div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo $reserve->req_trans_type; ?></div>
		                    </div>

                    	</div>
                    </div>
                    <div class="col-md-1">
                    	<div style="text-align: center;border-left: 5px solid #8BC34A;height: 275px;   margin-bottom: 25px;"></div>
                    </div>

                    <div class="col-md-6">
                    	<div class="col-md-12" style="margin-bottom: 30px;">
	                    	<h4 style="color: #fff;text-align: left;">ข้อมูลการชำระเงิน</h4>
	                    	<div class="row" style="text-align: center;">
	                    		<div class="col-md-12"  style="color: #fff;font-size: 15px;margin-bottom: 10px;">
	                    			<?php 
	                    				$image_show = "";
	                    				if(!empty($reserve->payment)){

	                    					$image_show = str_replace("/public","",$reserve->payment->pay_img);  
	                    				}
	                    			?>
	                    			<img src="<?php echo empty($image_show) ? url("")."/themes/img/not_found.png" : url("").$image_show; ?>" style="width: 45%;">
	                    		</div>
	                    		<div class="col-md-12">
		                    		<div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">จำนวนเงิน :</div>
	                        		<div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo empty($reserve->payment) ? "" : $reserve->payment->pay_money; ?></div>
	                        	</div>

                        		<div class="col-md-12">
	                        		<div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: right;margin-bottom: 10px;">ชำระผ่านทางธนาคาร :</div>
	                        		<div class="col-md-6"  style="color: #fff;font-size: 15px;text-align: left;margin-bottom: 10px;"><?php echo empty($reserve->payment) ? "" : $reserve->payment->pay_bank; ?></div>
	                        	</div>

	                    	</div>
	                   	</div>
                    </div>
                </div>

                    <!-- <div class="row">
	                   	<div class="col-md-12" style="margin-bottom: 30px;">
	                    	<h4 style="color: #fff">ข้อมูลการชำระเงิน</h4>
	                    	<div style="color: #fff;font-size: 15px;">บริการรับ-ส่ง : <span style="padding-left: 15px;"><?php //echo $reserve->req_trans_type; ?></span></div>
	                   	</div>
                    </div>
 -->
                    <div class="row" style="padding-bottom: 30px;">
                            <button type="submit" class="btn-fill btn-small btn-submit" data-id="<?php echo $reserve->payment->pay_id; ?>">อนุมัติ</button>
                            <button type="submit" class="btn-fill btn-small btn-back-home" >กลับ</button>
                    </div> 
            </div>

        </div>
    </div>
</div>

<div style="margin: 50px;"></div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('admin.ajax_center.post');?>"></div>

<!-- onclick="window.location.href = '/reserve/edit/'; " -->

<style type="text/css">
  
</style>

<script>
	$(function(){

	    $('.btn-submit').on('click', function(){

	        var pay_id = $(this).data('id');

	        swal({
	                title: "Are you sure?",
	                text: "Do you want approve this payment ?",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonClass: "btn-danger",
	                confirmButtonText: "Yes",
	                cancelButtonText: "No",
	                closeOnConfirm: true,
	                closeOnCancel: true
	            },
	            function(isConfirm) {
	                if (isConfirm) {
	                    msg_waiting();
	                    postApporvePayment(pay_id);
	                }
	         });
	    });

        $('.btn-back-home').on('click', function(){
           
            window.location.href = '/admin/detail'; 
        });

        $('.btn-back').on('click', function(){
           
            window.location.href = '/reserve/edit/'+ $(this).data('id'); 
        });
	});

	function postApporvePayment(pay_id){
		var data        = new FormData();
	    var ajax_url  	= $('#ajax-center-url').data('url');
	    var method      = 'postApporvePayment';
	   
	    data.append('method', method);
	    data.append('pay_id', pay_id);

	    $.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: ajax_url,
	        data: data,
	        contentType: false,
	        processData:false,
	        cache: false,
	        success: function(result) {
	            if(result.status == 'success'){
	                window.location.href = '/admin/detail'; 
	            } // End if check s tatus success.

	            if(result.status == 'error'){
	                
	            }
	        },
	        error : function(error) {
	            console.log(error);
	        }
	    });
	}
</script>
