<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-user {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-location-arrow{
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }
    .sign-up .signup-form .form-input-group i.fa-phone {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }
    .sign-up .signup-form .form-input-group i.fa-envelope-o {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-car {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }
</style>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="sign-up">
                <div class="col-12 signup-form" style="background-color: rgba(52, 50, 50, 0.29);border: 2px solid #14d21c;border-radius: 55px;">
                    <h2 style="color: #fff">เข้าสู่ระบบ</h2>
                    <div style="padding: 35px 0px;">
                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="กรุณากรอกชื่อผู้ใช้งานหรืออีเมล" name="username" value="">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-lock"></i><input type="password" class="" placeholder="กรุณากรอกรหัสผ่าน" name="password" value="">
                        </div>
                    </div>

                    <div style="padding-bottom: 30px;">
                        <button type="submit" class="btn-fill btn-small btn-login">เข้าสู่ระบบ</button>
                    </div> 
                </div>

                <hr />

				<div class="row">
                    <div class="col-xs-12 p-t-5">
                     <!-- Error Validation สำหรับ error message-->
                        <div class="alert alert-dismissible text-center err-pass">
                            <span id='message'></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="login_url" data-url="<?php echo \URL::route('auth.login.post'); ?>"></div>



<script>
	$(function(){

		$('.btn-login').on('click', function(){
            msg_waiting();

            var username = $('input[name=username]').val();
			var password = $('input[name=password]').val();
			var login_url = $('#login_url').data('url');

			// console.log(login_url +"  "+password+"  "+username);

			$.ajax({
				headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
				type: "POST",
				url: login_url,
				data: {username : username, password : password},
				success: function(Response) {

                    if(Response.status == 'success'){
                        if(Response.user_type == 'user'){
                            window.location.href = "/";
                        }else{
                            window.location.href = "/admin/dashboard";
                        }

                    }else{
                        msg_stop();
                        $('.err-pass').addClass('alert-danger');
                        $('#message').html(Response.msg).css('color', 'red');
                        console.log(Response.msg);
                    }

				}
			});
            
        });

	});
</script>