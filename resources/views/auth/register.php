
<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-user {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-location-arrow{
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-phone {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-envelope-o {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-car {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-calendar {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
</style>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="sign-up">
                <div class="col-12 signup-form" style="background-color: rgba(52, 50, 50, 0.29);border: 2px solid #14d21c;border-radius: 55px;">
                    <h2 style="color: #fff">สมัครสมาชิก</h2>
                    <div style="padding: 35px 0px;">
                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="กรุณากรอกชื่อ" name="firstname" value="">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="กรุณากรอกนามสกุล" name="lastname" value="">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-phone"></i><input type="text" class="" placeholder="กรุณากรอกเบอร์โทรศัพท์" name="tel" value="">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-lock"></i><input type="text" class="" placeholder="กรุณากรอกไอดีไลน์" name="line_id" value="">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-envelope-o"></i><input type="text" class="" placeholder="กรุณากรอกอีเมล" name="email" value="">
                        </div>

                        <!-- <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="กรุณากรอกชื่อผู้เข้าใช้" name="username" value="">
                        </div> -->

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-lock"></i><input type="password" class="" placeholder="กรุณากรอกรหัสผ่าน" name="password" value="">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-lock"></i><input type="password" class="" placeholder="กรุณายืนยันรหัสผ่าน" name="cf_pass" value="">
                        </div>
                    </div>

                    <div class="alert alert-dismissible text-center err-pass">
                        <span id='message'></span>
                    </div>

                    <div style="padding-bottom: 30px;">
                        <button type="submit" class="btn-fill btn-small btn-back" onclick="window.location.href = '/'; ">ยกเลิก</button>
                        <button type="submit" class="btn-fill btn-small btn-regiter">สมัครสมาชิก</button>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>

<div style="margin: 50px;"></div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="register_url" data-url="<?php echo \URL::route('auth.register.post'); ?>"></div>

<script>
	$(function(){

		$('input[name=password], input[name=cf_pass]').on('keyup', function () {
			$('.err-pass').addClass('alert-danger');
			$('#message').css('font-weight', 'bold');
			$('#message').css('font-size', '18px');
			if ($('input[name=password]').val() == $('input[name=cf_pass]').val()) {
				$('#message').html('Matching').css('color', 'green');
			} else 
				$('#message').html('Not Matching').css('color', 'red');
		});

		$('.btn-regiter').on('click', function(){
            msg_waiting();

            var firstname 	= $('input[name=firstname]').val();
			var lastname 	= $('input[name=lastname]').val();
			var tel 		= $('input[name=tel]').val();
			var line_id 	= $('input[name=line_id]').val();
			var email 		= $('input[name=email]').val();
			var username 	= $('input[name=username]').val();
			var password 	= $('input[name=password]').val();

			var register_url = $('#register_url').data('url');

			// console.log(login_url +"  "+password+"  "+username);

			// check format email
			var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

			if (testEmail.test(email)){
				$.ajax({
					headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
					type: "POST",
					url: register_url,
					data: {
						username : username,
						password : password,
						firstname : firstname,
						lastname : lastname,
						tel : tel,
						line_id : line_id,
						email : email,

					},
					success: function(Response) {
						window.location.href = "/auth/login";
					}
				});

			}else{
				msg_error_custom("Invalid Email", "Please check your email format.");
			}
            
        });

	});
</script>
