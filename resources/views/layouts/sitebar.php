<header>
    <div class="header-content">
        <div class="logo"><a href="/"><img src="/themes/img/logo/head-logo.png" alt="Sedna logo" width="30%"></a></div>
        <div class="header-nav">
            <nav>
                <ul class="primary-nav">
                    
                    <!-- <li><a href="/">ติดต่อเรา</a></li> -->
                    <?php if(!empty($userObject->user_type)): ?>
                        <?php if($userObject->user_type == 'user'): ?>
                            <li><a href="/">หน้าหลัก</a></li>
                            <li><a href="/service">บริการของเรา</a></li>
                            <li><a href="/my_pet">ข้อมูลสัตว์เลี้ยง</a></li>
                            <li><a href="/reserve/hitory">ประวัติการจอง</a></li>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if(!empty($userObject->user_type)): ?>
	                    <?php if($userObject->user_type == 'admin'): ?>
                            <li><a href="/admin/dashboard">หน้าหลัก</a></li>
                            <li><a href="/service">บริการของเรา</a></li>
                            <li><a href="/my_pet">ข้อมูลสัตว์เลี้ยง</a></li>
		                    <!-- <li><a href="/admin/dashboard">Dashboard</a></li> -->
		                    <li><a href="/admin/detail">รายละเอียดการจอง</a></li>
                            <!-- <li><a href="/">รายงาน</a></li> -->
		                    <li><a href="/admin/manage_user">จัดการผู้ใช้งาน</a></li>
	                    <?php endif ?>
                    <?php endif ?>

                    <?php if(empty($userObject)): ?>
                        <li><a href="/">หน้าหลัก</a></li>
                        <li><a href="/service">บริการของเรา</a></li>
                    <?php endif ?>

                    <!-- /admin/dashboard 
                    	/admin/detail
                        /contactus
                    -->
                </ul>
                <ul class="member-actions">
                	<?php if(!empty($userObject)): ?>
                        <?php 
                            $memberurl = "";
                            $keyname ="";
                            if($userObject->user_type == 'user'){
                                $memberurl = \URL::route('main.member.get');
                                $keyname = $userObject->firstname.' '. $userObject->lastname;
                            } elseif ($userObject->user_type == 'admin') {
                                $memberurl = \URL::route('manage_user.index.get');
                                $keyname = $userObject->firstname;
                            }
                        ?>
                		<li><a href="<?php echo $memberurl;?>" class="login"><?php echo $keyname; ?></a></li>
                    	<li><button class="btn-white btn-small btn-logout" style="padding: 5px 20px;">ออกจากระบบ</button></li>
                	<?php else: ?>
	                    <li><a href="/auth/login" class="login">เข้าสู่ระบบ</a></li>
	                    <li><a href="/auth/register" class="btn-white btn-small">สมัครสมาชิก</a></li>
                	<?php endif ?>
                </ul>
            </nav>
        </div>
        <div class="navicon">
            <a class="nav-toggle" href="#"><span></span></a>
        </div>
    </div>
</header>

<style type="text/css">
    header ul.primary-nav li a {
        color: #000;
        font-size: 16px;
    }

    header ul.member-actions li a{
        color: #000;
        font-size: 16px;
    }
</style>

<div id="logout-url"  data-url="<?php echo \URL::route('auth.logout.get');?>">