<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Hachi | Hachi Rabbit Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="/themes/img/iconpet/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/img/iconpet/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/img/iconpet/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/img/iconpet/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/img/iconpet/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/img/iconpet/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/img/iconpet/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/img/iconpet/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/img/iconpet/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/img/iconpet/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/img/iconpet/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/img/iconpet/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/img/iconpet/favicon-16x16.png">
    <link rel="manifest" href="/themes/img/iconpet/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/themes/img/iconpet/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="/themes/css/normalize.min.css">
    <link rel="stylesheet" href="/themes/css/bootstrap.min.css">
    <link rel="stylesheet" href="/themes/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/themes/css/flexslider.css">
    <link rel="stylesheet" href="/themes/css/styles.css">
    <link rel="stylesheet" href="/themes/css/queries.css">
    <link rel="stylesheet" href="/themes/css/etline-font.css">
    <link rel="stylesheet" href="/themes/bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- Sweetalert Css -->
    <link href="/themes/bower_components/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- bootstrap-material-datetimepicker Css -->
    <link href="/themes/bower_components/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="/themes/bower_components/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">



    <script src="/themes/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/themes/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="/themes/bower_components/retina.js/dist/retina.js"></script>
    <script src="/themes/js/jquery.fancybox.pack.js"></script>
    <script src="/themes/js/vendor/bootstrap.min.js"></script>
    <script src="/themes/js/scripts.js"></script>
    <script src="/themes/js/jquery.flexslider-min.js"></script>
    <script src="/themes/bower_components/classie/classie.js"></script>
    <script src="/themes/bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="/js/site.js"></script>

    <!-- Jquery Sweetalert Js -->
    <script src="/themes/bower_components/sweetalert/sweetalert.min.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/themes/bower_components/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/themes/bower_components/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

    <!-- datetimepicker Js -->
    <script src="/themes/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <!-- Bootbox Plugin Js -->
    <script src="/themes/bower_components/bootstrap-bootbox/bootbox.min.js"></script>

    <script type="text/javascript" src="/themes/bower_components/moment/min/moment.min.js"></script>

    <script type="text/javascript" src="/themes/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <link rel="stylesheet" href="/themes/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

</head>

<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }
</style>


<body id="top">
    <section class="hero">
        <section class="navigation">
            <?php echo view('layouts.sitebar',compact('userObject')); ?>
        </section>


        <div class="container">
            <?php  echo view($page, $data); ?>
        </div>
        <!-- <div class="down-arrow floating-arrow"><a href="#"><i class="fa fa-angle-down"></i></a></div> -->
    </section>
    
    
   
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="footer-links">
                        <h3 style="color: #000;font-weight: 800;">ติดต่อเรา</h3>
                        <div style="border-top: solid 1px #4E566C;"></div>
                        <!-- <ul class="footer-group">
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Sign up</a></li>
                            <li><a href="http://tympanus.net/codrops/licensing/">Licence</a></li>
                            <li><a href="http://tympanus.net/codrops/">Codrops</a></li>
                            <li><a href="http://www.peterfinlan.com/">Peter Finlan</a></li>
                        </ul> -->
                        <img src="/themes/img/logo/head-logo.png" style="width: 22%">
                        <div style="margin-bottom: 10px;font-size: 15px;color: #000;font-weight: 500px;">เปิดบริการทุกวัน จันทร์ - อาทิตย์ เวลา 09.00 น. - 22.00 น.</div>
                        <div style="font-size: 15px;color: #000;font-weight: 500px;margin-bottom: 10px;">tel. : <a href="tel:+669403392330">09403392330</a>, <a href="tel:+66837006067">0837006067</a></div>
                        <div style="">
                            <a href="http://line.me/ti/p/~phaiaor" class="twitter-share" target="_blank"><img src="/themes/img/icons8-line-100.png"></a> 

                            <a href="https://web.facebook.com/pages/category/Community/Hachi-Rabbit-Home-1537190153217737/?_rdc=1&_rdr" class="facebook-share" target="_blank"><img src="/themes/img/icons8-facebook-old-100.png"></a>
                        </div>
                        <p style="color: #000">Copyright © 2020 Hachi Rabbit Home<br>

                        <!--  -->
                        
                    </div>
                </div>
                <div class="col-md-3 social-share">
                    <div class="mapouter"><div class="gmap_canvas"><iframe width="95%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=60%2010%2F1%20Alley%2C%20%E0%B8%95%E0%B8%B3%E0%B8%9A%E0%B8%A5%20%E0%B8%9A%E0%B8%B2%E0%B8%87%E0%B8%9B%E0%B8%A3%E0%B8%AD%E0%B8%81%20%E0%B8%AD%E0%B8%B3%E0%B9%80%E0%B8%A0%E0%B8%AD%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%87%E0%B8%9B%E0%B8%97%E0%B8%B8%E0%B8%A1%E0%B8%98%E0%B8%B2%E0%B8%99%E0%B8%B5%20%E0%B8%9B%E0%B8%97%E0%B8%B8%E0%B8%A1%E0%B8%98%E0%B8%B2%E0%B8%99%E0%B8%B5%2012000&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:400px;width:95%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:95%;}</style></div>
                </div>
            </div>
        </div>
    </footer>
    
    
</body>
</html>


