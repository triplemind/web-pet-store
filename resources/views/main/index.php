<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            <img src="/themes/img/New Project.png" style="width: 70%">
            
            <div class="sign-up">
                <div class="col-12 signup-form" style="background-color: #fff;">
                    <h2 style="background-color: #bdbdbd;">ตรวจสอบวันว่าง</h2>
                    <div style="padding: 35px 0px;">
                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-calendar"></i><input type="text" class="date datetimepicker" placeholder="Enter your Birthday" name="birthday" value="">
                        </div>
                    </div>

                    <div style="padding-bottom: 30px;">
                        <button type="submit" class="btn-fill btn-small btn-sarch">ค้นหา</button>
                        <?php if(!empty($userObject)): ?>
                        <button type="submit" class="btn-fill btn-small btn-booking">จอง</button>
                        <?php endif ?>
                    </div> 
                    <?php if(!empty($userObject)): ?>
                        <?php if($getpets->count() == 0): ?>
                            <input type="hidden" name="pet_count" value="<?php echo $getpets->count(); ?>">
                            <div style="color: red; padding: 10px">
                                *** กรุณากรองข้อมูลสัตว์เลี้ยงก่อนทำการจอง ***
                            </div>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>

        </div>
    </div>
</div>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="ajax_center_url" data-url="<?php echo \URL::route('main.ajax_center.post'); ?>"></div>


<script>
	$(function(){

        // var action = 'open';
        // var event = 'main page';
        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: "POST",
            url: $('#ajax_center_url').data('url'),
            data: {
                method : 'getStatLoadpage',
            },
            success: function(Response) {
                // console.log("dsdsd");
            }
        });

		$('.datetimepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose : true,
        });

		$('.btn-booking').on('click', function(){ 
            var pet_count = $('input[name=pet_count]').val();

            if(pet_count == 0){
                swal({
                    title:'คุณยังไม่มีข้อมูลสัตว์เลี้ยงในระบบ', 
                    text: 'กรุณากรอกข้อมูลสัตว์เลี้ยงก่อนทำรายการ', 
                    type:'error', 
                    confirmButtonText:  'OK'},
                    function(){
                      window.location.href = "/my_pet";
                    });
            }else{
                window.location.href = "/reserve";
            }
        });


        $('.btn-sarch').on('click', function(){
            msg_waiting();
            var date                = $('.datetimepicker').val();
            var method              = 'getDatafromSearch';
            var ajax_center_url     = $('#ajax_center_url').data('url');

            if(date !== ""){
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: "POST",
                    url: ajax_center_url,
                    data: {
                        method : method,
                        date : date,

                    },
                    success: function(Response) {
                        console.log(Response.status);
                        console.log(Response.total_1);
                        console.log(Response.total_2);
                        console.log(Response.total_3);

                        if(Response.status == "success"){
                            // msg_stop();
                            var message = '<div><h4> วันที่ : '+date+'</h4>'
                                    +'<h3>จำนวนที่ว่าง</h3><h4 style="color: #ad0a0a;">'+Response.total_1+'</h4>'
                                    +'<div style="color: red;font-size:15px;">*** จำนวนพื้นที่วาง กรง/คอก ทั้งหมด 30 ที่ ***</div>'
                                    +'<hr style="border-top: 1px solid #bbb;">'
                                    +'<h3>จำนวนกรงที่ว่าง</h3>'
                                    +'<h4 style="color: #ad0a0a;">'+Response.total_2+'</h4>'
                                    +'<div style="color: red;font-size:15px;">*** จำนวนกรงทั้งหมด 5 ที่ ***</div>'
                                    +'<hr style="border-top: 1px solid #bbb;">'
                                    +'<h3>จำนวนคอกที่ว่าง</h3>'
                                    +'<h4 style="color: #ad0a0a;">'+Response.total_3+'</h4></div>'
                                    +'<div style="color: red;font-size:15px;">*** จำนวนคอกทั้งหมด 6 ที่ ***</div>';


                            swal({title: "ตรวจสอบวันว่าง", html: true, text:message});
                        }

                        if(Response.status == "error"){

                        }

                    }
                });

            }else{

                msg_unsuccess('กรุณาระบุวันที่ต้องการค้นหา !!');

            }
            
        });

	});
</script>