
<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-user {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-location-arrow{
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-phone {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-envelope-o {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-car {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-calendar {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
</style>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="sign-up">
                <div class="col-12 signup-form" style="background-color: rgba(52, 50, 50, 0.29);border: 2px solid #14d21c;border-radius: 55px;">
                    <h2 style="color: #fff">ข้อมูลส่วนตัว</h2>
                    <div style="padding: 35px 0px;">
                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="กรุณากรอกชื่อ" name="firstname" value="<?php echo empty($member->firstname) ? "" : $member->firstname; ?>">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="กรุณากรอกนามสกุล" name="lastname" value="<?php echo empty($member->lastname) ? "" : $member->lastname; ?>">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-phone"></i><input type="text" class="" placeholder="กรุณากรอกเบอร์โทรศัพท์" name="tel" value="<?php echo empty($member->tel) ? "" : $member->tel; ?>">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-lock"></i><input type="text" class="" placeholder="กรุณากรอกไอดีไลน์" name="line_id" value="@<?php echo empty($member->line_id) ? "" : $member->line_id; ?>">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-envelope-o"></i><input type="text" class="" placeholder="กรุณากรอกอีเมล" name="email" value="<?php echo empty($member->email) ? "" : $member->email; ?>">
                        </div>

                        <!-- <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-user"></i><input type="text" class="" placeholder="กรุณากรอกชื่อผู้เข้าใช้" name="username" value="<?php //echo empty($member->username) ? "" : $member->username; ?>">
                        </div> -->

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-lock"></i><input type="password" class="" placeholder="กรุณากรอกรหัสผ่าน" name="password" value="">
                        </div>

                        <div class="form-input-group" style="width: 45%; height: 45px;">
                            <i class="fa fa-lock"></i><input type="password" class="" placeholder="กรุณายืนยันรหัสผ่าน" name="cf_pass" value="">
                        </div>
                    </div>

                    <div class="alert alert-dismissible text-center err-pass">
                        <span id='message'></span>
                    </div>

                    <div style="padding-bottom: 30px;">
                        <button type="submit" class="btn-fill btn-small btn-back" onclick="window.location.href = '/'; ">ยกเลิก</button>
                        <button type="submit" class="btn-fill btn-small btn-save" data-id="<?php echo empty($member->id) ? "" : $member->id; ?>">บันทึกข้อมูล</button>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>

<div style="margin: 50px;"></div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="edit_url" data-url="<?php echo \URL::route('main.member.post'); ?>"></div>

<script>
	$(function(){

		$('input[name=password], input[name=cf_pass]').on('keyup', function () {
			$('.err-pass').addClass('alert-danger');
			$('#message').css('font-weight', 'bold');
			$('#message').css('font-size', '18px');
			if ($('input[name=password]').val() == $('input[name=cf_pass]').val()) {
				$('#message').html('Matching').css('color', 'green');
			} else 
				$('#message').html('Not Matching').css('color', 'red');
		});

		$('.btn-save').on('click', function(){
            msg_waiting();

            var id 			= $(this).data('id');
            var firstname 	= $('input[name=firstname]').val();
			var lastname 	= $('input[name=lastname]').val();
			var tel 		= $('input[name=tel]').val();
			var line_id 	= $('input[name=line_id]').val();
			var email 		= $('input[name=email]').val();
			var username 	= $('input[name=username]').val();
			var password 	= $('input[name=password]').val();

			var edit_url 	= $('#edit_url').data('url');

			// console.log(login_url +"  "+password+"  "+username);

			// check format email
			var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

			if (testEmail.test(email)){
				$.ajax({
					headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
					type: "POST",
					url: edit_url,
					data: {
						id : id,
						username : username,
						password : password,
						firstname : firstname,
						lastname : lastname,
						tel : tel,
						line_id : line_id,
						email : email,

					},
					success: function(Response) {
						window.location.href = "/admin/manage_user";
					}
				});

			}else{
				msg_error_custom("Invalid Email", "Please check your email format.");
			}
            
        });

	});
</script>
