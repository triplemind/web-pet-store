
<style type="text/css">
    
</style>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="">
                <div class="col-12 signup-form" style="background-color: rgba(52, 50, 50, 0.61);border: 2px solid #14d21c;border-radius: 55px;">
                    <h2 style="color: #fff">รายละเอียดการจอง</h2>
                    <div class="col-md-12" style="margin-bottom: 30px;">
                        <div style="color: #fff">
                            <h4>เงินค่ามัดจำ</h4>
                            <div style="margin-right: 20%;padding: 10px;background-color: #00000087;  margin-left: 20%;font-size: 18px;">ยอดชำระทั้งหมด 300 บาท</div>
                        </div>

                        <div style="color: #fff">
                            <h4>เลขที่บัญชีธนาคาร</h4>
                            <div style="margin-right: 20%;padding: 10px;background-color: #00000087;  margin-left: 20%;font-size: 18px;">
                                <div>ธนาคารกสิกรไทย (KBank)</div>
                                <div>ชื่อบัญชี : กัลยรัตน์ เทพชา</div>
                                <div>เลขที่บัญชี : 744-284727-9</div>
                            </div>
                        </div>
                    </div>

                    <div style="padding-bottom: 30px;">
                        <button type="submit" class="btn-fill btn-small btn-back" >กลับ</button>
                        <button class="btn-fill btn-small btn-submit" data-toggle="modal" data-target="#myModal">อัพโหลดหลักฐานการโอน</button>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>

<div style="margin: 50px;"></div>



<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body" style="margin-bottom: 40%;">

                <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <label style="color:#000;font-size: 17px">วันที่โอน : </label>
                    </div>
                    <div class="col-md-8" style="margin-bottom: 20px;">
                        <input type="text" class="date datetimepicker form-control" name="payment_date" value="" placeholder="กรุณาเลือกวันที่">
                    </div>
                </div>

                <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <label style="color:#000;font-size: 17px">เวลาที่โอน : </label>
                    </div>
                    <div class="col-md-8" style="margin-bottom: 20px;">
                        <input type='text' class="form-control datetimepicker3" name="payment_time" value="" placeholder="กรุณาเลือกเวลา" />
                    </div>
                </div>

                <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <label style="color:#000;font-size: 17px">จำนวนเงิน : </label>
                    </div>
                    <div class="col-md-8" style="margin-bottom: 20px;">
                        <input type="text" class="form-control" name="cash" value="">
                    </div>
                </div>

                <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <label style="color:#000;font-size: 17px">ธนาคาร : </label>
                    </div>
                    <div class="col-md-8" style="margin-bottom: 20px;">
                        <select class="form-control" name="bank">
                            <option value='' selected>--- กรุณาเลือกธนาคาร ---</option>
                            <option value='ธนาคารกรุงเทพ'>ธนาคารกรุงเทพ</option>
                            <option value='ธนาคารกสิกรไทย'>ธนาคารกสิกรไทย</option>
                            <option value='ธนาคารกรุงไทย'>ธนาคารกรุงไทย</option>
                            <option value='ธนาคารทหารไทย'>ธนาคารทหารไทย</option>
                            <option value='ธนาคารไทยพาณิชย์'>ธนาคารไทยพาณิชย์</option>
                            <option value='ธนาคารกรุงศรีอยุธยา'>ธนาคารกรุงศรีอยุธยา</option>
                            <option value='ธนาคารเกียรตินาคิน'>ธนาคารเกียรตินาคิน</option>
                            <option value='ธนาคารซีไอเอ็มบีไทย'>ธนาคารซีไอเอ็มบีไทย</option>
                            <option value='ธนาคารทิสโก้'>ธนาคารทิสโก้</option>
                            <option value='ธนาคารธนชาต'>ธนาคารธนชาต</option>
                            <option value='ธนาคารยูโอบี'>ธนาคารยูโอบี</option>
                            <option value='ธนาคารออมสิน'>ธนาคารออมสิน</option>
                            <option value='ธนาคารอาคารสงเคราะห์'>ธนาคารอาคารสงเคราะห์</option>
                            <option value='ธนาคารอิสลามแห่งประเทศไทย'>ธนาคารอิสลามแห่งประเทศไทย</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                    <div class="col-md-4" style="margin-bottom: 20px;">
                        <label style="color:#000;font-size: 17px">หลักฐานการโอน : </label>
                    </div>
                    <div class="col-md-8" style="margin-bottom: 20px;">
                        <input type="file" class="form-control" name="img_slip" value="">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-fill btn-send" data-dismiss="modal" data-reserve_id="<?php echo $id; ?>">ส่งหลักฐาน</button>
            </div>
        </div>

    </div>
</div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="add_url" data-url="<?php echo \URL::route('payment.add.post'); ?>"></div>
<!-- <div id="edit_url" data-url="<?php //echo \URL::route('pet.index.post'); ?>"></div>
<div id="remove_url" data-url="<?php //echo \URL::route('pet.index.post'); ?>"></div> -->

<!-- onclick="window.location.href = '/reserve/edit/'; " -->

<style type="text/css">
  .bootstrap-datetimepicker-widget .btn{
        color: #204d74 !important;
        border: solid 0px;
    }

    .bootstrap-datetimepicker-widget .btn:hover{
        color: #204d74 !important;
        border: solid 0px;
        background-color: #fff
    }

    .bootstrap-datetimepicker-widget .btn:focus{
        color: #204d74 !important;
        border: solid 0px;
        background-color: #fff
    }

    .btn-primary
    {
        display:block;
        border-radius:0px;
        box-shadow:0px 4px 6px 2px rgba(0,0,0,0.2);
        margin-top:-5px;
    }
</style>

<script>
	$(function(){

        $('.datetimepicker3').datetimepicker({
            format: 'LT'
        });

        $('.datetimepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose : true,
        });
        $('.btn-send').on('click', function(){
            msg_waiting();
            var reserve_id = $(this).data('reserve_id')
            console.log($(this).data('reserve_id'));
            postAddPayment(reserve_id);
        });

        $('.btn-back').on('click', function(){
           
            window.location.href = '/reserve/hitory'; 
        });
	});


    function postAddPayment(reserve_id){
        var payment_date    = $('input[name=payment_date]').val();
        var payment_time    = $('input[name=payment_time]').val();
        var cash            = $('input[name=cash]').val();
        var bank            = $('select[name=bank]').val();

        var add_url     = $('#add_url').data('url');
        var data        = new FormData();

        if (typeof($('input[name=img_slip]')[0]) !== 'undefined') {

            jQuery.each(jQuery('input[name=img_slip]')[0].files, function(i, file) {
                data.append('img_slip', file);
            });
        }

        data.append('reserve_id', reserve_id);
        data.append('payment_date', payment_date);
        data.append('payment_time', payment_time);
        data.append('cash', cash);
        data.append('bank', bank);

        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: "POST",
            url: add_url,
            data:data,
            contentType: false,
            processData:false,
            cache: false,
            success: function(Response) {

                swal({
                        title: "Good job!", 
                        text: "You clicked the button!", 
                        type: "success",
                        confirmButtonText:  'OK',
                    },function(){
                        console.log("dsdsdsd");
                        window.location.href = "/";
                });
            }
        });


    }
</script>
