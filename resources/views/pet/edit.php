
<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-user {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-location-arrow{
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-phone {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-envelope-o {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-car {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-calendar {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
</style>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="" style="text-align: center;">
                <div class="col-12 signup-form" style="background-color: rgba(52, 50, 50, 0.29);border: 2px solid #14d21c;border-radius: 55px;">
                    <h2 style="color: #fff">ข้อมูลสัตว์เลี้ยง</h2>
                    <div style="padding: 35px 0px;">
                        
                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                        	<div class="col-md-4" style="margin-bottom: 20px;">
                        		<label style="color:#fff;font-size: 17px">ชื่อสัตว์เลี้ยง : </label>
                        	</div>
                        	<div class="col-md-8" style="margin-bottom: 20px;">
                            	<input type="text" class="form-control" name="pet_name" value="<?php echo $getpet->pet_name ?>">
                        	</div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                        	<div class="col-md-4" style="margin-bottom: 20px;">
                        		<label style="color:#fff;font-size: 17px">เพศสัตว์เลี้ยง : </label>
                        	</div>
                        	<div class="col-md-8" style="margin-bottom: 20px;">
                            	<select class="form-control" name="pet_sex">
                            		<option value="male" <?php echo ($getpet->pet_sex == "male") ? "selected" : ""; ?> >ตัวผู้</option>
                            		<option value="female" <?php echo ($getpet->pet_sex == "female") ? "selected" : ""; ?> >ตัวเมีย</option>
                            	</select>
                        	</div>
                        </div>

                      <!--   rabbit 		=>กระต่าย
                		cavy		=>แก้สบี้
                		hamster		=>แฮมเตอร์
                		porcupine	=>เม่นแคระ -->

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                        	<div class="col-md-4" style="margin-bottom: 20px;">
                        		<label style="color:#fff;font-size: 17px">ประเภทสัตว์เลี้ยง : </label>
                        	</div>
                        	<div class="col-md-8" style="margin-bottom: 20px;">
                            	<select class="form-control" name="type_id">
                            		<option value="1" <?php echo ($getpet->type_id == 1) ? "selected" : ""; ?> >กระต่าย</option>
                            		<option value="2" <?php echo ($getpet->type_id == 2) ? "selected" : ""; ?> >แก้สบี้</option>
                            		<option value="3" <?php echo ($getpet->type_id == 3) ? "selected" : ""; ?> >แฮมเตอร์</option>
                            		<option value="4" <?php echo ($getpet->type_id == 4) ? "selected" : ""; ?> >เม่นแคระ</option>
                            	</select>
                        	</div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                        	<div class="col-md-4" style="margin-bottom: 20px;">
                        		<label style="color:#fff;font-size: 17px">สายพันธุ์ : </label>
                        	</div>
                        	<div class="col-md-8" style="margin-bottom: 20px;">
                            	<input type="text" class="form-control" name="pet_remark" value="<?php echo $getpet->pet_remark ?>">
                        	</div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                            <div class="col-md-4" style="margin-bottom: 20px;">
                                <label style="color:#fff;font-size: 17px">รูปสัตว์เลี้ยง : </label>
                            </div>
                            <div class="col-md-8" style="margin-bottom: 20px;">
                                <input type="file" class="form-control" name="pet_img" value="">
                            </div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                        	<div class="col-md-4" style="margin-bottom: 20px;">
                        		<label style="color:#fff;font-size: 17px">อาการป่วย : </label>
                        	</div>
                        	<div class="col-md-8" style="margin-bottom: 20px;">
                            	<textarea rows="6" class="form-control" name="pet_sick" ><?php echo $getpet->pet_sick ?></textarea> 
                        	</div>
                        </div>




                        <!-- <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                        	<div class="col-md-4">
                        		<label style="color:#fff;font-size: 17px">รายละเอียดอื่นๆ : </label>
                        	</div>
                        	<div class="col-md-8">
                            	<textarea rows="6" class="form-control" name="pet_remark" ></textarea> 
                        	</div>
                        </div> -->

                        <div style="color: red;font-weight: bold;">*****กรุณากรอกข้อมูลให้ครบ*****</div>
                    </div>

                    <div style="padding-bottom: 30px;">
                        <button type="submit" class="btn-fill btn-small btn-back" onclick="window.location.href = '/my_pet'; ">ยกเลิก</button>
                        <button type="submit" class="btn-fill btn-small btn-save" data-pet_id="<?php echo $getpet->pet_id ?>">บันทึกข้อมูล</button>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>

<div style="margin: 50px;"></div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="add_url" data-url="<?php echo \URL::route('pet.add.post'); ?>"></div>
<div id="edit_url" data-url="<?php echo \URL::route('pet.edit.post'); ?>"></div>
<div id="remove_url" data-url="<?php echo \URL::route('pet.remove.post'); ?>"></div>

<script>
	$(function(){

		$('.btn-save').on('click', function(){
            msg_waiting();
            var pet_id 	    = $(this).data('pet_id');
            var pet_name    = $('input[name=pet_name]').val();
			var pet_sex 	= $('select[name=pet_sex]').val();
			var type_id 	= $('select[name=type_id]').val();
			var pet_remark 	= $('input[name=pet_remark]').val();
			var pet_sick 	= $('textarea[name=pet_sick]').val();

			var edit_url 	= $('#edit_url').data('url');
            var data        = new FormData();

            if (typeof($('input[name=pet_img]')[0]) !== 'undefined') {

                jQuery.each(jQuery('input[name=pet_img]')[0].files, function(i, file) {
                    data.append('pet_img', file);
                });
            }

            data.append('pet_id', pet_id);
            data.append('pet_name', pet_name);
            data.append('pet_sex', pet_sex);
            data.append('type_id', type_id);
            data.append('pet_remark', pet_remark);
            data.append('pet_sick', pet_sick);


			$.ajax({
				headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
				type: "POST",
				url: edit_url,
				data: data,
                contentType: false,
                processData:false,
                cache: false,
				success: function(Response) {
					window.location.href = "/my_pet";
				}
			});
            
        });

	});
</script>
