<style>
    .card {
        box-shadow: 0 4px 8px 0 rgba(50, 116, 100, 0.75);
        transition: 0.3s;
        /*width: 40%;*/
        border: 3px solid #28bd2e;
        background-color: #fff;
    }

    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(50, 116, 100, 0.75);
    }

    .text-car{
        color: #525252;
        padding-bottom: 0px;
        line-height: 15px;
    }

    .containerdddd {
        padding: 2px 16px;
    }

    .btn-remove{
        color: #fff;
        border: solid 2px #ff3d00;
        border-radius: 40px;
        display: inline-block;
        text-transform: uppercase;
        background-color: #F44336;
    }

    .btn-remove:hover{
        border: solid 2px #a72800;
        background-color: #f44336d1;
    }


    .btn-edit{
        color: #fff;
        border: solid 2px #ff3d00;
        border-radius: 40px;
        display: inline-block;
        text-transform: uppercase;
        background-color: #FF5722;
    }

    .btn-edit:hover{
        border: solid 2px #a72800;
        background-color: #c54117;
    }

    .btn-warning:focus{
        background-color: #ff9e14;
        border-color: #ff9e14;
    }
    .btn-danger:focus{
        background-color: #ff2e28;
        border-color: #ff2e28;
    }
    .btn-primary:focus{
        background-color: #0a80e6;
        border-color: #0a80e6;
    }
    .btn-danger:hover{
        background-color: #ff2e28;
        border-color: #ff2e28;
    }
</style>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <!-- <h2 style="color: #fff">ข้อมูลสัตว์เลี้ยง</h2> -->
            <div style="color: #000;text-align: left;border-bottom: solid 1px #000;font-size: 22px;margin-bottom: 20px;">ข้อมูลสัตว์เลี้ยง</div>
            <div class="col-12" >
                <div class="col-md-6 col-md-offset-3 sign-up">
                    <div style="text-align: right;">
                        <button type="submit" id="btn-add" style="padding: 10px 20px;font-size: 20px;" class="btn-fill btn-small " >+</button>
                    </div>

                    <?php if($getpets->count() != 0): ?>
                        <?php foreach ($getpets as $key => $getpet): ?>

                            <div class=".col-md-4 .col-md-offset-4" style="padding: 5% 0%;">
                                <div class="card">
                                    <div class="containerdddd">
                                    	<?php $petimg = str_replace("/public","",$getpet->pet_img); ?>
                                    	<div><img width="50%" src="<?php echo empty($getpet->pet_img) ? url("")."/themes/img/not_found.png" :  url("").$petimg; ?>"></div>
                                        <div>
                                            <h4 style="color: #525252;"><b><?php echo $getpet->pet_name ?></b></h4>
                                        </div>
                                        <p class="text-car"><?php echo $getpet->pet_sex ?></p> 
                                        <p class="text-car"><?php echo $getpet->pet_remark ?></p> 
                                        <!-- <p class="text-car"><?php //echo $getpet->pet_sick ?></p>  -->

                                        <div style="text-align: right;margin-bottom: 10px;margin-top: -10%;">
                                            <button type="submit" id="btn-edit" style="padding: 10px 13px;font-size: 11px;" class="btn-edit" data-pet_id="<?php echo $getpet->pet_id ?>"><i class="fa fa-pencil"></i></button>
                                            <button type="submit" id="btn-remove" style="padding: 10px 13px;font-size: 11px;" class="btn-remove" data-pet_id="<?php echo $getpet->pet_id ?>" data-name="<?php echo $getpet->pet_name ?>"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach ?>
                    <?php endif ?>

                    <?php if($getpets->count() == 0): ?>
                        <div class=".col-md-4 .col-md-offset-4" style="padding: 5% 0%;">
                            <div class="card">
                                <div class="containerdddd">
                                    <div>
                                        <h4 style="color: #525252;"><b>กรุณาเพิ่มข้อมูลสัตว์เลี้ยง !!!</b></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>


                    <div style="margin-top: 30px;margin-bottom: 30px;">
                        <button type="submit" class="btn-white btn-small btn-back">ย้อนกลับ</button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div style="margin: 50px;"></div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="remove_url" data-url="<?php echo \URL::route('pet.remove.post'); ?>"></div>

<script>
	$(function(){
		$('.btn-back').on('click', function(){
			window.location.href = "/";
		});

		$('.btn-edit').on('click', function(){
			var pet_id = $(this).data('pet_id');
			window.location.href = "/my_pet/edit/"+pet_id;
		});

		$('#btn-add').on('click', function(){
			window.location.href = "/my_pet/add";
		});

		$('.btn-remove').on('click', function(){
			var pet_id = $(this).data('pet_id');
			var name = $(this).data('name');

			swal({
                  title: "Are you sure?",
                  text: "Do you want delete => "+name+" ?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: true,
                  closeOnCancel: true
                },
                  function(isConfirm) {
                  if (isConfirm) {
                        msg_waiting();
                        postRemove(pet_id);

                  }
            });
		});

	});


	function postRemove(pet_id)
    {
        var remove_url      = $('#remove_url').data('url');

        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: 'post',
            url: remove_url,
            data: {
                 'pet_id' : pet_id,
            },
            success: function(result) {
                if(result.status == 'success'){
                    window.location.reload();
                } // End if check s tatus success.
            },
            error : function(error) {
                msg_error_custom('Error!', error);
            }
        });
    }
</script>
