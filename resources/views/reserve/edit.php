
<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-user {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-location-arrow{
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-phone {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-envelope-o {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-car {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-calendar {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
</style>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="" style="text-align: center;">
                <div class="col-12 signup-form" style="background-color: rgba(52, 50, 50, 0.61);border: 2px solid #14d21c;border-radius: 55px;">
                    <h2 style="color: #fff">แก้ไขข้อมูลการจอง</h2>
                    <div style="padding: 35px 0px;">

                              


                        <input type="hidden" name="reserve_id" value="<?php echo $reserve->reserve_id ?>">
                        
                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                        	<div class="col-md-4" style="margin-bottom: 20px;">
                        		<label style="color:#fff;font-size: 17px">Check-in : </label>
                        	</div>
                        	<div class="col-md-8" style="margin-bottom: 20px;">
                            	<input type="text" class="date datetimepicker form-control" name="reserve_date_chkin" value="<?php echo $reserve->reserve_date_chkin ?>" >
                        	</div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                            <div class="col-md-4" style="margin-bottom: 20px;">
                                <label style="color:#fff;font-size: 17px">Check-out : </label>
                            </div>
                            <div class="col-md-8" style="margin-bottom: 20px;">
                               <input type="text" class="date datetimepicker form-control" name="reserve_date_chkout" value="<?php echo $reserve->reserve_date_chkout ?>" >
                            </div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                            <div class="col-md-4" style="margin-bottom: 20px;">
                                <label style="color:#fff;font-size: 17px">เวลานำสัตว์เลี้ยงเข้าฝาก : </label>
                            </div>
                            <div class="col-md-8" style="margin-bottom: 20px;">
                                <!-- <input type="text" class="date datetimepicker form-control" name="reserve_date_chkout" value="" placeholder="กรุณาเลือกวันที่"> -->
                                <input type='text' class="form-control datetimepicker3" name="reserve_date" value="<?php echo $reserve->reserve_date ?>" />
                            </div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                            <div class="col-md-4" style="margin-bottom: 20px;">
                                <label style="color:#fff;font-size: 17px">ประเภทที่พัก : </label>
                            </div>
                            <div class="col-md-8" style="margin-bottom: 20px;">
                                <select class="form-control" name="rest_id">
                                    <option value="1" <?php echo ($reserve->rest_id == 1) ? "selected" : "" ?> >กรง</option>
                                    <option value="2" <?php echo ($reserve->rest_id == 2) ? "selected" : "" ?> >คอก</option>
                                    <option value="3" <?php echo ($reserve->rest_id == 3) ? "selected" : "" ?> >พื้นที่วาง กรง/คอก ส่วนตัว</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                            <div class="col-md-4" style="">
                                <label style="color:#fff;font-size: 17px">สัตว์เลี้ยง : </label>
                            </div>
                            <div class="col-md-8" style="margin-bottom: 20px;">
                                <?php $pet_name = json_decode($reserve->pet_id, true); ?>
                                <select class="form-control pet_name" name="pet_name[]" multiple="multiple">
                                    <?php if(!empty($pets)): ?>
                                        <?php foreach($pets as $key => $pet): ?>
                                        <option value="<?php echo $pet->pet_id ?>" <?php echo in_array($pet->pet_id, $pet_name) ? "selected" : "" ?> ><?php echo $pet->pet_name ?></option>
                                    <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>
                        </div>

                        <!-- <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                            <div class="col-md-4" style="margin-bottom: 20px;">
                                <label style="color:#fff;font-size: 17px">จำนวน : </label>
                            </div>
                            <div class="col-md-8" style="margin-bottom: 20px;">
                                <input type="text" class="form-control" name="count" value="">
                            </div>
                        </div> -->

                        <div class="col-12 form-input-group" style="/*width: 45%; height: 45px;*/">
                            <div class="col-md-4" style="margin-bottom: 20px;">
                                <label style="color:#fff;font-size: 17px">บริการรับ-ส่ง : </label>
                            </div>
                            <div class="col-md-8" style="margin-bottom: 20px;">
                                <select class="form-control" name="req_trans_type">
                                    <option value="yes" <?php echo ($reserve->req_trans_type == "yes") ? "selected" : "" ?> >ใช้บริการรับ-ส่ง</option>
                                    <option value="no" <?php echo ($reserve->req_trans_type == "no") ? "selected" : "" ?> >ไม่ใช้บริการรับ-ส่ง</option>
                                </select>
                            </div>
                        </div>

                        <div style="color: red;font-weight: bold;">*****กรุณากรอกข้อมูลให้ครบ*****</div>
                    </div>

                    <div style="padding-bottom: 30px;">
                        <button type="submit" class="btn-fill btn-small btn-back" onclick="window.location.href = '/'; ">ยกเลิก</button>
                        <button type="submit" class="btn-fill btn-small btn-save">บันทึกข้อมูล</button>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>

<div style="margin: 50px;"></div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<!-- <div id="add_url" data-url="<?php //echo \URL::route('reserve.index.post'); ?>"></div> -->
<div id="edit_url" data-url="<?php echo \URL::route('reserve.edit.post'); ?>"></div>
<!-- <div id="remove_url" data-url="<?php //echo \URL::route('pet.index.post'); ?>"></div> -->

<style type="text/css">
    .bootstrap-datetimepicker-widget .btn{
        color: #204d74 !important;
        border: solid 0px;
    }

    .bootstrap-datetimepicker-widget .btn:hover{
        color: #204d74 !important;
        border: solid 0px;
        background-color: #fff
    }

    .bootstrap-datetimepicker-widget .btn:focus{
        color: #204d74 !important;
        border: solid 0px;
        background-color: #fff
    }

    .btn-primary
    {
        display:block;
        border-radius:0px;
        box-shadow:0px 4px 6px 2px rgba(0,0,0,0.2);
        margin-top:-5px;
    }

</style>

<script>
	$(function(){

        $('.datetimepicker3').datetimepicker({
            format: 'LT'
        });

        $('.pet_name').select2();

        $('.datetimepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose : true,
        });

		$('.btn-save').on('click', function(){
            msg_waiting();
            var reserve_id 	            = $('input[name=reserve_id]').val();
            var reserve_date_chkin      = $('input[name=reserve_date_chkin]').val();
			var reserve_date_chkout 	= $('input[name=reserve_date_chkout]').val();
			var reserve_date 	        = $('input[name=reserve_date]').val();
			var rest_id 	            = $('select[name=rest_id]').val();
            var pet_name                = $('.pet_name').val();
            var count                   = $('input[name=count]').val();
			var req_trans_type 	        = $('select[name=req_trans_type]').val();

            console.log(pet_name);

			var edit_url 	= $('#edit_url').data('url');

			$.ajax({
				headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
				type: "POST",
				url: edit_url,
				data: {
					reserve_id : reserve_id,
                    reserve_date_chkin : reserve_date_chkin,
					reserve_date_chkout : reserve_date_chkout,
					reserve_date : reserve_date,
					rest_id : rest_id,
					pet_name : pet_name,
                    count : count,
                    req_trans_type : req_trans_type,

				},
				success: function(Response) {
                    if(Response.status == "success"){
					   window.location.href = "/reserve/detail/"+Response.id;
                    }
				}
			});
            
        });

	});
</script>
