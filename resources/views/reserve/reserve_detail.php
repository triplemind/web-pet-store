
<style type="text/css">
    .sign-up .signup-form .form-input-group i.fa-calendar {
        font-size: 18px;
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 20px;
    }

    .sign-up .signup-form .form-input-group i.fa-user {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-location-arrow{
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-phone {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
	.sign-up .signup-form .form-input-group i.fa-envelope-o {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-car {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}

	.sign-up .signup-form .form-input-group i.fa-calendar {
	    font-size: 18px;
	    position: absolute;
	    top: 50%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	    margin-left: 20px;
	}
</style>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="hero-content text-center ">
            
            <div class="">
                <div class="col-12 signup-form" style="background-color: rgba(52, 50, 50, 0.61);border: 2px solid #14d21c;border-radius: 55px;">
                    <h2 style="color: #fff">รายละเอียดการจอง</h2>
                    
                    <div class="col-md-10 col-md-offset-1" style="margin-bottom: 30px;">

                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">รหัสการจอง : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo "00".$reserve->reserve_id; ?></div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">รหัส กรง, คอก, พื้นที่วาง กรง/คอก ส่วนตัว : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo $name_no; ?></div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">รหัสสมาชิก : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo "00".$reserve->user_id; ?></div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">ชื่อสัตว์เลี้ยง : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;">
                                <?php 
                                    $pet_id = json_decode($reserve->pet_id);

                                    foreach ($pets as $key => $pet){
                                        if(in_array($pet->pet_id, $pet_id)){
                                            echo $pet->pet_name.", ";
                                        }
                                    }
                                ?>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">ชื่อ-สกุล : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo !empty($reserve->user) ? $reserve->user->firstname." ".$reserve->user->lastname : ""; ?></div>
                        </div>



                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">Check-in : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo $reserve->reserve_date_chkin; ?></div>
                        </div>



                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">Check-out : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo $reserve->reserve_date_chkout; ?></div>
                        </div>




                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">เวลานำสัตว์เข้าฝาก : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo $reserve->reserve_date; ?></div>
                        </div>




                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">ประเภทที่พัก : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo !empty($reserve->rest) ? $reserve->rest->rest_name : ""; ?></div>
                        </div>



                        <div class="col-md-12">
                            <div class="col-md-6" style="color: #fff;text-align: right;font-size: 15px;">บริการรับ-ส่ง : </div>
                            <div class="col-md-6" style="color: #fff;text-align: left;font-size: 15px;"><?php echo $reserve->req_trans_type; ?></div>
                        </div>

                    </div>

                    <div style="padding-bottom: 30px;">
                        <?php if(!empty($reserve->payment)): ?>
                            <?php if($reserve->payment->pay_status == "pending"): ?>
                                <button type="submit" class="btn-fill btn-small btn-back" data-id="<?php echo $reserve->reserve_id; ?>">แก้ไขข้อมูล</button>
                                <button type="submit" class="btn-fill btn-small btn-submit" data-id="<?php echo $reserve->reserve_id; ?>">ยืนยัน</button>
                            <?php endif ?>
                        <?php endif ?>
                            <button type="submit" class="btn-fill btn-small btn-back-home" >กลับ</button>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>

<div style="margin: 50px;"></div>


<!-- <input type="hidden" name="_token" id="csrf-token" value="<?php //echo csrf_token() ?>" /> -->
<!-- <div id="add_url" data-url="<?php //echo \URL::route('pet.add.post'); ?>"></div> -->
<!-- <div id="edit_url" data-url="<?php //echo \URL::route('pet.index.post'); ?>"></div>
<div id="remove_url" data-url="<?php //echo \URL::route('pet.index.post'); ?>"></div> -->

<!-- onclick="window.location.href = '/reserve/edit/'; " -->

<style type="text/css">
  
</style>

<script>
	$(function(){
        $('.btn-submit').on('click', function(){
            window.location.href = '/payment/'+ $(this).data('id'); 
            // window.location.reload(); 
        });

        $('.btn-back-home').on('click', function(){
           
            window.location.href = '/reserve/hitory'; 
        });

        $('.btn-back').on('click', function(){
           
            window.location.href = '/reserve/edit/'+ $(this).data('id'); 
        });
	});
</script>
