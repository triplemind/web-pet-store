<div class="row">
    <div class="col-md-12">
        <div class="hero-content text-center ">
            
            <!-- <h1 class="text-center" style="color: #000;font-weight: bold;">ประวัติการจอง</h1> -->
            <div style="color: #000;text-align: left;border-bottom: solid 1px #000;font-size: 22px;margin-bottom: 20px;">ประวัติการจอง</div>

            <div class="col-12">
                <div class="col-md-12" style="margin-bottom: 20%;">
                    <table class="table table-striped table-hover js-basic-example dataTable table-responsive" style="background-color: #dedfee;">
                        <thead>
                            <th>#</th>
                            <th>ชื่อสัตว์เลี้ยง</th>
                            <th>Checkin Date</th>
                            <th>Checkout Date</th>
                            <th>บริการรับ - ส่ง</th>
                            <th>ประเภทที่พัก</th>
                            <th>สถานะการชำระเงิน</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <?php if(!empty($reserves)):?>
                                <?php foreach ($reserves as $key => $reserve):?>
                                    <tr>
                                        <td><?php echo $key+1 ?></td>
                                        <td>
                                            <?php 
                                                $pet_id = json_decode($reserve->pet_id);

                                                foreach ($pets as $key => $pet){
                                                    if(in_array($pet->pet_id, $pet_id)){
                                                        echo $pet->pet_name.", ";
                                                    }
                                                }

                                            ?>
                                        </td>
                                        <td><?php echo $reserve->reserve_date_chkin ?></td>
                                        <td><?php echo $reserve->reserve_date_chkout ?></td>
                                        <td><?php echo $reserve->req_trans_type ?></td>
                                        <td><?php echo !empty($reserve->rest) ? $reserve->rest->rest_name : "" ?></td>
                                        <td>
                                            <?php 
                                                $status = "";

                                                if(!empty($reserve->payment)){

                                                    if($reserve->payment->pay_status == "pending"){
                                                        $status = "รอดำเนินการ";
                                                    }elseif($reserve->payment->pay_status == "waiting"){
                                                        $status = "เจ้าหน้าที่กำลังตรวจสอบ";
                                                    }elseif($reserve->payment->pay_status == "completed"){
                                                        $status = "ดำเนินการเสร็จสิ้น";
                                                    }

                                                }

                                                echo $status;
                                            ?>
                                                
                                        </td>

                                        <td width="15%">
                                            <div class="row text-center">
                                                <button class=" btn btn-warning btn-edit"  data-reserve_id="<?php echo $reserve->reserve_id; ?>">
                                                        <!-- <i class="fa fa-pencil-square-o"></i> -->
                                                    <span>ตรวจสอบ</span>
                                                </button>
                                                <?php if($reserve->payment_status == "pending"): ?>
                                                    <button class=" btn btn-fill btn-payment"  data-reserve_id="<?php echo $reserve->reserve_id; ?>">
                                                            <!-- <i class="fa fa-pencil-square-o"></i> -->
                                                        <span>จ่ายเงิน</span>
                                                    </button>
                                                <?php endif ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>

                        </tbody>
                    </table>
                    <!-- แสดงตัวเลข page -->
                    <?php //echo $reserves->render(); ?>

                    <?php if($reserves->count() == 0):?>
                        <div class=".col-md-4 .col-md-offset-4" style="padding: 5% 0%;">
                            <div class="card">
                                <div class="containerdddd">
                                    <div>
                                        <h4 style="color: #525252;"><b>ไม่พบข้อมูลการจอง !!!</b></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                    
                </div>
            </div>

        </div>
    </div>
</div>

<style type="text/css">
    .btn-warning:hover{
        border-color: #ffa72b;
        background-color: #ffa72b;
    }
    .btn-warning:focus{
        border-color: #ffa72b;
        background-color: #ffa72b;
    }
    .card {
        box-shadow: 0 4px 8px 0 rgba(50, 116, 100, 0.75);
        transition: 0.3s;
        /*width: 40%;*/
        border: 3px solid #28bd2e;
        background-color: #fff;
    }
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(50, 116, 100, 0.75);
    }
    .containerdddd {
        padding: 2px 16px;
    }
</style>


<script>
    $(function(){

    // CALL Data Table
    $('.dataTables_length').css('display', 'none');
    $('.dataTables_paginate').css('display', 'none');

    // 

    // EDIT USER
    $('.js-basic-example').on('click', '.btn-edit', function(){

        window.location.href = "/reserve/detail/"+$(this).data('reserve_id');
    });

    $('.js-basic-example').on('click', '.btn-payment', function(){

        window.location.href = "/payment/"+$(this).data('reserve_id');
    });


});



</script>