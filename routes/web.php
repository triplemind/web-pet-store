<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.template');
// });

// main page
Route::get('/', [
	'uses'  => 'MainController@Index',
	'as'  => 'main.index.get'
]);

// หน้าบริการของเรา
Route::get('/service', [
	'uses'  => 'MainController@getService',
	'as'  => 'service.index.get'
]);

// หน้า user profile
Route::get('/profile', [
	'uses'  => 'MainController@getProfile',
	'as'  => 'main.member.get'
]);

Route::post('/profile', [
	'uses'  => 'MainController@postEditProfile',
	'as'  => 'main.member.post'
]);

Route::post('/ajax_center', [
	'uses'  => 'MainController@ajaxCenter',
	'as'  => 'main.ajax_center.post'
]);


// หน้าจัดการสัตว์เลี้ยง
Route::get('/my_pet', [
	'uses'  => 'PetController@Index',
	'as'  => 'pet.index.get'
]);

// หน้าจัดการสัตว์เลี้ยง
Route::get('/my_pet/add', [
	'uses'  => 'PetController@getAdd',
	'as'  => 'pet.index.get'
]);

Route::get('/my_pet/edit/{id}', [
	'uses'  => 'PetController@getEdit',
	'as'  => 'pet.index.get'
]);

Route::post('/my_pet/add', [
	'uses'  => 'PetController@postAdd',
	'as'  => 'pet.add.post'
]);

Route::post('/my_pet/edit', [
	'uses'  => 'PetController@postEdit',
	'as'  => 'pet.edit.post'
]);

Route::post('/my_pet/remove', [
	'uses'  => 'PetController@postRemove',
	'as'  => 'pet.remove.post'
]);


Route::get('/reserve', [
	'uses'  => 'ReserveController@Index',
	'as'  => 'reserve.index.get'
]);

Route::get('/reserve/edit/{id}', [
	'uses'  => 'ReserveController@getEdit',
	'as'  => 'reserve.edit.get'
]);

Route::get('/reserve/hitory', [
	'uses'  => 'ReserveController@getReserveHistory',
	'as'  => 'reserve.hitory.get'
]);

Route::get('/reserve/detail/{id}', [
	'uses'  => 'ReserveController@getReserveDetail',
	'as'  => 'reserve.detail.get'
]);

Route::post('/reserve', [
	'uses'  => 'ReserveController@postAdd',
	'as'  => 'reserve.index.post'
]);

Route::post('/reserve/edit', [
	'uses'  => 'ReserveController@postEdit',
	'as'  => 'reserve.edit.post'
]);


// PAYMENT
Route::get('/payment/{id}', [
	'uses'  => 'PaymentController@Index',
	'as'  => 'payment.index.get'
]);
Route::post('/payment/add', [
	'uses'  => 'PaymentController@postAddPayment',
	'as'  => 'payment.add.post'
]);




// AUTH

// LOGIN
Route::get('/auth/login', [
	'uses'	=> 'Auth\LoginController@getLogin',
	'as'	=> 'auth.login.get'
]);

Route::post('/auth/login', [
	'uses'	=> 'Auth\LoginController@postLogin',
	'as'	=> 'auth.login.post'
]);
// LOGIN

// LOGOUT
Route::get('/auth/logout', [
	'uses'	=> 'Auth\LoginController@logout',
	'as'	=> 'auth.logout.get'
]);
// LOGOUT


// REGISTER
Route::get('/auth/register', [
	'uses'	=> 'Auth\RegisterController@getRegister',
	'as'	=> 'auth.register.get'
]);

Route::post('/auth/register', [
  'uses'  => 'Auth\RegisterController@postRegister',
  'as'  => 'auth.register.post'
]);
// REGISTER



Route::get('/admin/manage_user', [
	'uses'	=> 'ManageUserController@Index',
	'as'	=> 'manage_user.index.get'
]);

Route::post('/admin/manage_user/edit', [
  'uses'  => 'ManageUserController@postEdit',
  'as'  => 'manage_user.edit.post'
]);

Route::post('/admin/manage_user/delete', [
  'uses'  => 'ManageUserController@postRemove',
  'as'  => 'manage_user.delete.post'
]);

Route::post('/admin/manage_user/ajax_center', [
  'uses'  => 'ManageUserController@ajaxCenter',
  'as'  => 'manage_user.ajax_center.post'
]);




Route::get('/admin/dashboard', [
	'uses'	=> 'AdminController@getDashboard',
	'as'	=> 'dashboard.index.get'
]);

Route::get('/admin/detail', [
	'uses'	=> 'AdminController@getDetail',
	'as'	=> 'detail.index.get'
]);

Route::post('/admin/detail/remove', [
  'uses'  => 'AdminController@postRemoveReserve',
  'as'  => 'detail.remove.post'
]);

Route::get('/admin/payment_detail/{id}', [
	'uses'	=> 'AdminController@getPaymentDetail',
	'as'	=> 'payment_detail.index.get'
]);

Route::post('/admin/ajax_center', [
  'uses'  => 'AdminController@ajaxCenter',
  'as'  => 'admin.ajax_center.post'
]);




